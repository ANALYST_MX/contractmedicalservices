#include "Contract.hxx"

Contract::Contract(QSqlDatabase *db, const QString &path, QObject *parent)
    : QObject(parent), m_pDatabase(db), m_sPathTemplate(path)
{
    m_sOutputPathDirectory = QDir::tempPath();
    m_pWebView = new QWebView;
    m_pWebView->setVisible(false);
}

Contract::~Contract()
{
    delete m_pWebView;
    delete m_pPerson;
    delete m_pContractNumerator;
    m_pWebView = nullptr;
    m_pPerson = nullptr;
    m_pContractNumerator = nullptr;
}

void Contract::setClient(Person *client)
{
    m_pPerson = client;
}

void Contract::setCountMedicalServices(int countMedicalServices)
{
    m_iCountMedicalServices = countMedicalServices;
}

void Contract::addUsluga(MedicalServiceEntry *usluga)
{
    m_pUslugi.append(usluga);
}

void Contract::printPreview(bool isPrintOut)
{
    try
    {
        WordDocument doc(FileDocument(m_sPathTemplate).copyToDir(m_sOutputPathDirectory));
        doc.open();
        doc.substitution("{{contract_date}}", m_dateContractDate.toString("dd.MM.yyyy"));
        doc.substitution("{{contract_number}}", generateContractNumber());
        doc.substitution("{{surname}}", m_pPerson->getSurname());
        doc.substitution("{{name}}", m_pPerson->getName());
        doc.substitution("{{patronymic}}", m_pPerson->getPatronymic());
        doc.substitution("{{birthday}}", m_pPerson->getBirthday().toString("dd.MM.yyyy"));
        doc.substitution("{{address}}", m_pPerson->getAddress());
        doc.substitution("{{phone}}", m_pPerson->getPhone());
        Document *document = m_pPerson->getDocument();
        doc.substitution("{{document_number}}", document->getNumber());
        doc.substitution("{{document_serial}}", document->getSerial());
        doc.substitution("{{document_date_of_issue}}", document->getDateOfIssue().toString("dd.MM.yyyy"));
        doc.substitution("{{document_authority}}", document->getAuthority());
        doc.substitution("{{document_type}}", document->getType().second);
        if (m_pPerson->getRepresentative() != nullptr)
        {
            doc.substitution("{{representative_surname}}", m_pPerson->getRepresentative()->getSurname());
            doc.substitution("{{representative_name}}", m_pPerson->getRepresentative()->getName());
            doc.substitution("{{representative_patronymic}}", m_pPerson->getRepresentative()->getPatronymic());
            doc.substitution("{{representative_birthday}}", m_pPerson->getRepresentative()->getBirthday().toString("dd.MM.yyyy"));
            doc.substitution("{{representative_address}}", m_pPerson->getRepresentative()->getAddress());
            doc.substitution("{{representative_phone}}", m_pPerson->getRepresentative()->getPhone());
            Document *representativeDocument = m_pPerson->getRepresentative()->getDocument();
            doc.substitution("{{representative_document_number}}", representativeDocument->getNumber());
            doc.substitution("{{representative_document_serial}}", representativeDocument->getSerial());
            doc.substitution("{{representative_document_date_of_issue}}", representativeDocument->getDateOfIssue().toString("dd.MM.yyyy"));
            doc.substitution("{{representative_document_authority}}", representativeDocument->getAuthority());
            doc.substitution("{{representative_document_type}}", representativeDocument->getType().second);

            doc.substitution("{{surname|representative_surname}}", m_pPerson->getRepresentative()->getSurname());
            doc.substitution("{{name|representative_name}}", m_pPerson->getRepresentative()->getName());
            doc.substitution("{{patronymic|representative_patronymic}}", m_pPerson->getRepresentative()->getPatronymic());
            doc.substitution("{{birthday|representative_birthday}}", m_pPerson->getRepresentative()->getBirthday().toString("dd.MM.yyyy"));
            doc.substitution("{{address|representative_address}}", m_pPerson->getRepresentative()->getAddress());
            doc.substitution("{{phone|representative_phone}}", m_pPerson->getRepresentative()->getPhone());
            doc.substitution("{{document_number|representative_document_number}}", representativeDocument->getNumber());
            doc.substitution("{{document_serial|representative_document_serial}}", representativeDocument->getSerial());
            doc.substitution("{{document_date_of_issue|representative_document_date_of_issue}}", representativeDocument->getDateOfIssue().toString("dd.MM.yyyy"));
            doc.substitution("{{document_authority|representative_document_authority}}", representativeDocument->getAuthority());
            doc.substitution("{{document_type|representative_document_type}}", representativeDocument->getType().second);
        } else {
            doc.substitution("{{representative_surname}}", "");
            doc.substitution("{{representative_name}}", "");
            doc.substitution("{{representative_patronymic}}", "");
            doc.substitution("{{representative_birthday}}", "");
            doc.substitution("{{representative_address}}", "");
            doc.substitution("{{representative_phone}}", "");
            doc.substitution("{{representative_document_number}}", "");
            doc.substitution("{{representative_document_serial}}", "");
            doc.substitution("{{representative_document_date_of_issue}}", "");
            doc.substitution("{{representative_document_authority}}", "");
            doc.substitution("{{representative_document_type}}", "");

            doc.substitution("{{surname|representative_surname}}", m_pPerson->getSurname());
            doc.substitution("{{name|representative_name}}", m_pPerson->getName());
            doc.substitution("{{patronymic|representative_patronymic}}", m_pPerson->getPatronymic());
            doc.substitution("{{birthday|representative_birthday}}", m_pPerson->getBirthday().toString("dd.MM.yyyy"));
            doc.substitution("{{address|representative_address}}", m_pPerson->getAddress());
            doc.substitution("{{phone|representative_phone}}", m_pPerson->getPhone());
            doc.substitution("{{document_number|representative_document_number}}", document->getNumber());
            doc.substitution("{{document_serial|representative_document_serial}}", document->getSerial());
            doc.substitution("{{document_date_of_issue|representative_document_date_of_issue}}", document->getDateOfIssue().toString("dd.MM.yyyy"));
            doc.substitution("{{document_authority|representative_document_authority}}", document->getAuthority());
            doc.substitution("{{document_type|representative_document_type}}", document->getType().second);
        }
        double totalCost{ 0. };
        int n{ 1 };
        for (auto usluga : m_pUslugi)
        {
            if (n == m_iCountMedicalServices)
            {
                break;
            }
            doc.substitution("{{usluga_code_" + QString::number(n) + "}}", usluga->getCode());
            doc.substitution("{{usluga_name_" + QString::number(n) + "}}", usluga->getName());
            doc.substitution("{{usluga_count_" + QString::number(n) + "}}", QString::number(usluga->getCount()));
            doc.substitution("{{usluga_date_expires_" + QString::number(n) + "}}", usluga->getDateExpires().toString("dd.MM.yyyy"));
            doc.substitution("{{usluga_price_" + QString::number(n) + "}}", formatCurrency(usluga->getPrice()));
            double cost = usluga->getPrice() * usluga->getCount();
            doc.substitution("{{currency_symbol_" + QString::number(n) + "}}", m_sCurrencySymbol);
            doc.substitution("{{usluga_cost_" + QString::number(n) + "}}", formatCurrency(cost));
            totalCost += cost;
            ++n;
        }
        for (; n < m_iCountMedicalServices; ++n)
        {
            doc.substitution("{{usluga_code_" + QString::number(n) + "}}", "");
            doc.substitution("{{usluga_name_" + QString::number(n) + "}}", "");
            doc.substitution("{{usluga_count_" + QString::number(n) + "}}", "");
            doc.substitution("{{usluga_date_expires_" + QString::number(n) + "}}", "");
            doc.substitution("{{usluga_price_" + QString::number(n) + "}}", "");
            doc.substitution("{{currency_symbol_" + QString::number(n) + "}}", "");
            doc.substitution("{{usluga_cost_" + QString::number(n) + "}}", "");
        }
        doc.substitution("{{total_cost}}", formatCurrency(totalCost));
        doc.save();
        if (isPrintOut)
        {
            doc.setVisible(false);
            doc.print();
            doc.close();
        }
        else
        {
            doc.setVisible(true);
        }
    }
    catch (const std::bad_alloc &)
    {
    }
}

void Contract::setContractNumerator(ContractNumerator *contractNumerator)
{
    m_pContractNumerator = contractNumerator;
}

void Contract::setDateContract(QDate contractDate)
{
    m_dateContractDate = contractDate;
}

QString Contract::generateContractNumber()
{
    return m_pContractNumerator->getSerial() + QString("%1").arg(save(), m_pContractNumerator->getNumberLength(), 10, QChar('0'));
}

QString Contract::formatCurrency(double price)
{
    return QString::number(price, 'f', m_iCurrencyDecimalPlaces) + " " + m_sCurrencySymbol;
}

int Contract::save()
{
    int res = -1;
    if (m_pDatabase->isOpen())
    {
        m_pDatabase->database().transaction();
        QSqlQuery query;
        query.prepare(queryInsertContract);
        query.bindValue(":created_on", QDate::currentDate().toString("dd.MM.yyyy"));
        bool succeeded = query.exec();
        if(!succeeded)
        {
            m_pDatabase->database().rollback();
        }
        else
        {
            query.first();
            res = query.value("contract_number").toInt();
        }
        query.finish();
        m_pDatabase->database().commit();
    }

    return res;
}

void Contract::setOutputPathDirectory(const QString &tempDir)
{
    m_sOutputPathDirectory = tempDir;
}

void Contract::setCurrencyDecimalPlaces(int decimalPlaces)
{
    m_iCurrencyDecimalPlaces = decimalPlaces;
}

void Contract::setCurrencySymbol(const QString &symbol)
{
    m_sCurrencySymbol = symbol;
}
