#pragma once

#include <QNetworkReply>

class PromedAPI : public QObject
{
    Q_OBJECT
public:
    PromedAPI(QObject * = nullptr);

public:
    static bool checkNetworkError(QNetworkReply *);
};
