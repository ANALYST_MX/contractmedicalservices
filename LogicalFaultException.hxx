#pragma once

#include <stdexcept>

class LogicalFaultException : public std::logic_error
{
public:
    LogicalFaultException(const std::string &);
    virtual ~LogicalFaultException() throw();
};
