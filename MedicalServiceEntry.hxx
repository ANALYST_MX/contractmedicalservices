#pragma once

#include <QObject>
#include <QString>
#include <QDate>

class MedicalServiceEntry : public QObject
{
    Q_OBJECT
public:
    MedicalServiceEntry(QObject * = nullptr);
    void setCode(const QString &);
    void setName(const QString &);
    void setPrice(const double);
    void setCount(const int);
    void setDateExpires(const QDate &);
    QString getCode() const;
    QString getName() const;
    double getPrice() const;
    int getCount() const;
    QDate getDateExpires() const;
    MedicalServiceEntry *&clone() const;

private:
    QString m_sCode;
    QString m_sName;
    double m_dPrice;
    int m_iCount;
    QDate m_dateExpires;
};
