#pragma once

#include <QHash>
#include <QWidget>
#include <QtWidgets>
#include "MainWindow.hxx"
#include "EnumHelpers.hxx"

EnumClass(Departments, quint8,
          HOSPITAL,
          POLICLINIC,
          )

class GroupBoxDepartment : public QGroupBox
{
                                      Q_OBJECT

                                      public:
                                      GroupBoxDepartment(QWidget *pwgt = nullptr);
Departments getValue() const;
const QHash<Departments, QString> DepartmentKeysStrings();

public slots:
void selectDepartment();

private:
QRadioButton *createRadioButton(const QString &, const char *, bool = false);
Departments dep;
};
