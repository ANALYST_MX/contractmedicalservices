#pragma once

#include <QString>
#include <QJsonObject>
#include <QVariant>
#include <QDateTime>

class PromedJson : public QObject
{
    Q_OBJECT
public:
    PromedJson(QObject * = nullptr);

public:
    static QVariant objectValue(const QJsonObject &, const QString &, const QVariant = QVariant());
    static QVariant parse(const QString &);
    static QVariant parse(const QString &, bool &);
    static QByteArray serialize(const QVariant &);
    static QByteArray serialize(const QVariant &, bool &);
    static QString serializeStr(const QVariant &);
    static QString serializeStr(const QVariant &, bool &);
    static void setDateTimeFormat(const QString &);
    static void setDateFormat(const QString &);
    static QString getDateTimeFormat();
    static QString getDateFormat();

private:
    template<typename T>
    static QByteArray serializeMap(const T &, bool &);
    static QString sanitizeString(QString);
    static QByteArray join(const QList<QByteArray> &, const QByteArray &);
    static QVariant parseValue(const QString &, int &, bool &);
    static QVariant parseObject(const QString &, int &, bool &);
    static QVariant parseArray(const QString &, int &, bool &);
    static QVariant parseString(const QString &, int &, bool &);
    static QVariant parseNumber(const QString &, int &);
    static int lastIndexOfNumber(const QString &, int);
    static void eatWhitespace(const QString &json, int &index);
    static int lookAhead(const QString &, int);
    static int nextToken(const QString &, int &);
};
