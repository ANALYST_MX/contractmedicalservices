#include "GroupBoxDepartment.hxx"

GroupBoxDepartment::GroupBoxDepartment(QWidget *parent)
    : QGroupBox(parent), dep(Departments::POLICLINIC)
{
    setChecked(true);
    QHBoxLayout *phbxLayout = new QHBoxLayout;
    for (auto it = DepartmentKeysStrings().constBegin(); it != DepartmentKeysStrings().constEnd(); ++it)
    {
        phbxLayout->addWidget(createRadioButton(it.value(), SLOT(selectDepartment()), (it.key() == dep ? true : false)));
    }

    setLayout(phbxLayout);
}

QRadioButton *GroupBoxDepartment::createRadioButton(const QString &txt, const char *method, bool checked)
{
    QRadioButton *prad = new QRadioButton(txt);
    prad->setChecked(checked);
    connect(prad, SIGNAL(clicked()), method);

    return prad;
}

Departments GroupBoxDepartment::getValue() const
{
    return dep;
}

void GroupBoxDepartment::selectDepartment()
{
    for (auto it = DepartmentKeysStrings().constBegin(); it != DepartmentKeysStrings().constEnd(); ++it)
    {
        if (((QRadioButton *)sender())->text() == it.value())
        {
            dep = it.key();
        }
    }
}

const QHash<Departments, QString> GroupBoxDepartment::DepartmentKeysStrings()
{
    static const QHash<Departments, QString> departmentKeysStrings = QHash<Departments, QString>({
                                                                                                     {Departments::HOSPITAL, "Стационар"},
                                                                                                     {Departments::POLICLINIC, "Поликлиника"},
                                                                                                 });

    return departmentKeysStrings;
}
