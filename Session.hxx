#pragma once

#include <QApplication>

#include "PortableSleep.hxx"
#include "Configure.hxx"
#include "AuthPromed.hxx"

class Session : public QObject
{
    Q_OBJECT
public:
    explicit Session(AuthPromed *, QObject * = nullptr);

signals:
    void finished(bool error, bool aborted, const QString &text);

public slots:
    void keepIn();
    void keepOut();
    void reloadPage();
    void reloadPageFinished();

private:
    AuthPromed *m_pAuth;
    unsigned int m_iPeriod;
    bool m_bRunning;
    Configure *m_pConf;
};
