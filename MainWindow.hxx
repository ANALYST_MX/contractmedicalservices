#pragma once

#include <QMainWindow>
#include <QTabWidget>
#include <QtWidgets>
#include <QPair>
#include <QHash>
#include <QMessageBox>

#include "CartMedicalServiceList.hxx"
#include "MedicalServiceList.hxx"
#include "SettingAuthWindow.hxx"
#include "SettingDBWindow.hxx"
#include "SettingPrintWindow.hxx"
#include "SettingNumeratorWindow.hxx"
#include "DBConnection.hxx"
#include "AuthPromed.hxx"
#include "Person.hxx"
#include "PersonPromed.hxx"
#include "Session.hxx"
#include "Contract.hxx"

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget * = nullptr);
    ~MainWindow();
    void view();

public slots:
    void showSettingAuth(bool = false);
    void showSettingDB(bool = false);
    void showSettingPrintFileTemplate(bool = false);
    void showSettingNumeratorInterval(bool = false);
    void personInformationFound();
    void personInformationNotFound();
    void findByFIOAndBirthday();
    void searchUslugi();
    void selectUslugaFromFindedUslugiTable(int, int);
    void selectUslugaFromCartUslugiTable(int, int);
    void addUslugaToCartUslugiTable();
    void removeUslugafromCartUslugiTable();
    void print();
    void reset();

protected:
    bool eventFilter(QObject *, QEvent *);

private:
    AuthPromed *m_pAuth;
    Session *m_pSession;
    PersonPromed *m_pPromed;
    DBConnection *db;
    QPair<QLabel *, QLineEdit *> *createTextField(const QString &);
    QPair<QLabel *, QDateEdit *> *createDateField(const QString &);
    QPair<QLabel *, QComboBox *> *createComboBox(const QString &);
    QPair<QLabel *, QSpinBox *> *createSpinField(const QString &, int = 0, int = 512);
    QPair<QLabel *, QCheckBox *> *createCheckBox(const QString &, bool);
    QPushButton *createButton(const QString &, const char *);
    QTableWidget *createTable(int, int, const char *);
    QLabel *createLabel(const QString &);
    void createMenus();
    void showFindedUslugi(MedicalServiceList *);
    void showCartUslugi();
    void showTotalCost();
    bool checkConfig();
    QGridLayout *pgrdLayout;
    QTabWidget *m_ptabPersons;
    QPushButton *m_pcmdSearchPerson;
    QCheckBox *m_pchkPrintOutContract;
    QDateEdit *m_pdateContractDateField;
    QLineEdit *m_ptxtSearchClientSurnameField;
    QLineEdit *m_ptxtSearchClientNameField;
    QLineEdit *m_ptxtSearchClientPatronymicField;
    QDateEdit *m_pdateSearchClientBirthdayField;
    QLineEdit *m_ptxtSearchClientAddressField;
    QLineEdit *m_ptxtSearchClientPhoneField;
    QComboBox *m_pcboSearchClientDocumentTypeField;
    QLineEdit *m_ptxtSearchClientDocumentSerialField;
    QLineEdit *m_ptxtSearchClientDocumentNumberField;
    QLineEdit *m_ptxtSearchClientDocumentAuthorityField;
    QDateEdit *m_pdateSearchClientDateOfIssueField;
    QLineEdit *m_ptxtSearchRepresentativeSurnameField;
    QLineEdit *m_ptxtSearchRepresentativeNameField;
    QLineEdit *m_ptxtSearchRepresentativePatronymicField;
    QDateEdit *m_pdateSearchRepresentativeBirthdayField;
    QLineEdit *m_ptxtSearchRepresentativeAddressField;
    QLineEdit *m_ptxtSearchRepresentativePhoneField;
    QComboBox *m_pcboSearchRepresentativeDocumentTypeField;
    QLineEdit *m_ptxtSearchRepresentativeDocumentSerialField;
    QLineEdit *m_ptxtSearchRepresentativeDocumentNumberField;
    QLineEdit *m_ptxtSearchRepresentativeDocumentAuthorityField;
    QDateEdit *m_pdateSearchRepresentativeDateOfIssueField;
    QLineEdit *m_ptxtSearchUslugiField;
    QLineEdit *m_ptxtEditUslugaIDField;
    QLineEdit *m_ptxtEditUslugaCodeField;
    QLineEdit *m_ptxtEditUslugaNameField;
    QLineEdit *m_ptxtEditUslugaPriceField;
    QSpinBox *m_pspbEditUslugaCountField;
    QDateEdit *m_pdateEditUslugaDateExpiresField;
    QTableWidget *m_ptblFindedUslugiTable;
    QTableWidget *m_ptblCartUslugiTable;
    QLabel *m_plblTotalCostUslugi;
    CartMedicalServiceList *m_pCart;
    std::vector<QObject *> translateToRus;
};

#include "SettingAuthWindow.hxx"
