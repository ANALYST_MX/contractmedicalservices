#pragma once

#include <QString>
#include <QFile>
#include <QDir>
#include <QTemporaryFile>

class FileDocument : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString path READ getPath WRITE setPath)
public:
    explicit FileDocument(const QString &, QObject * = nullptr);
    virtual ~FileDocument();
    virtual QString copyToDir(const QString &);
    virtual QString getPath() const;
    virtual void setPath(const QString &);
private:
    QString m_sPath;
};
