#include "App.hxx"

App::App(int &argc, char **argv, const QString &strOrg, const QString &strAppName)
    : QApplication(argc, argv), m_pSettings(nullptr)
{
    setOrganizationName(strOrg);
    setApplicationName(strAppName);
    m_pSettings = new QSettings(strOrg, strAppName, this);
}

App *App::theApp()
{
    return (App *)qApp;
}

QSettings *App::getSettings()
{
    return m_pSettings;
}
