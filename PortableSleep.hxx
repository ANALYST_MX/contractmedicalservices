#pragma once

#include <cstdint>

#ifdef _WIN32
#  include <windows.h>
#else
#  include <unistd.h>
#endif

class PortableSleep
{
public:
    static void sleepMillis(uint32_t);
    static void sleepSeconds(uint8_t);
    static void sleepMinutes(uint8_t);
    static void sleepHours(uint8_t);
};
