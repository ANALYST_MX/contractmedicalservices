#include "Document.hxx"

const QMap<int, QString> Document::TYPES{
    {0, "Не известо"},
    {1, "Паспорт гражданина СССР"},
    {2, "Загранпаспорт гражданина СССР"},
    {3, "Свидетельство о рождении Российский Федерации"},
    {4, "Удостоверение личности офицера"},
    {5, "Справка об освобождении из места лишения свободы"},
    {6, "Паспорт Минморфлота"},
    {7, "Военный билет"},
    {8, "Дипломатический паспорт гражданина Российской Федерации"},
    {9, "Иностранный паспорт"},
    {10, "Свидетельство о регистрации ходатайства о признании иммигранта беженцем"},
    {11, "Вид на жительство"},
    {12, "Удостоверение беженца в Российской Федерации"},
    {13, "Временное удостоверение личности гражданина Российской Федерации"},
    {14, "Паспорт гражданина Российской Федерации"},
    {15, "Заграничный паспорт гражданина Российской Федерации"},
    {15, "Паспорт моряка"},
    {17, "Военный билет офицера запаса"},
    {18, "Иные документы"},
    {21, "Документ иностранного гражданина"},
    {22, "Документ лица без гражданства"},
    {23, "Разрешение на временное проживание"},
    {24, "Свидетельство о рождении, выданное не в Российской Федерации"},
    {25, "Свидетельство о предоставлении временного убежища на территории РФ"},
    {26, "Удостоверение сотрудника Евразийской экономической комиссии"},
    {27, "Копия жалобы о лишении статуса беженца"},
    {28, "Иной документ, соответствующий свид-ву о предост. убежища на территории РФ"}
};

Document::Document(QObject *parent)
    : QObject(parent)
{

}

QString Document::getSerial() const
{
    return m_sSerial;
}

QString Document::getNumber() const
{
    return m_sNumber;
}

QString Document::getAuthority() const
{
    return m_sAuthority;
}

QDate Document::getDateOfIssue() const
{
    return m_dateOfIssue;
}

QDate Document::getDateOfExpiration() const
{
    return m_dateOfExpiration;
}

QPair<int, QString> Document::getType() const
{
    QPair<int, QString> documentType;
    documentType.first = m_iType;
    if (1 <= m_iType && 18 >= m_iType)
    {
        documentType.second = TYPES[m_iType];
    }
    else if (21 <= m_iType && 28 >= m_iType)
    {
        documentType.second = TYPES[m_iType - 2];
    }
    else
    {
        documentType.second = TYPES[0];
    }

    return documentType;
}

int Document::getAuthorityId() const
{
    return m_iAuthorityId;
}

void Document::setSerial(const QString &serial)
{
    m_sSerial = serial;
}

void Document::setNumber(const QString &number)
{
    m_sNumber = number;
}

void Document::setAuthority(const QString &authority)
{
    m_sAuthority = authority;
}

void Document::setDateOfIssue(const QDate &dateOfIssue)
{
    m_dateOfIssue = dateOfIssue;
}

void Document::setDateOfExpiration(const QDate &dateOfExpiration)
{
    m_dateOfExpiration = dateOfExpiration;
}

void Document::setType(int key)
{
    m_iType = key;
}

void Document::setAuthorityId(int authorityId)
{
    m_iAuthorityId = authorityId;
}

QMap<int, QString> Document::types()
{
    return TYPES;
}

QString Document::type(int key)
{
    return TYPES[key];
}

Document *&Document::clone() const
{
    Document *document = new Document;
    document->setAuthority(m_sAuthority);
    document->setAuthorityId(m_iAuthorityId);
    document->setDateOfExpiration(m_dateOfExpiration);
    document->setDateOfIssue(m_dateOfIssue);
    document->setType(m_iType);
    document->setNumber(m_sNumber);
    document->setSerial(m_sSerial);

    return document;
}

