#include "Person.hxx"

Person::Person(QObject *parent)
    : QObject(parent), m_pRepresentative(nullptr)
{
    m_sSurname = "";
    m_sName = "";
    m_sPatronymic = "";
    m_sAddress = "";
    m_sPhone = "";
    m_dateBirthday = QDate::currentDate();
}

int Person::getId() const
{
    return m_iId;
}

QString Person::getSurname() const
{
    return m_sSurname;
}

QString Person::getName() const
{
    return m_sName;
}

QString Person::getPatronymic() const
{
    return m_sPatronymic;
}

QDate Person::getBirthday() const
{
    return m_dateBirthday;
}

QString Person::getAddress() const
{
    return m_sAddress;
}

QString Person::getPhone() const
{
    return m_sPhone;
}

Document *&Person::getDocument() const
{
    return m_pDocument->clone();
}

const Person *Person::getRepresentative()
{
    return m_pRepresentative;
}

void Person::setSurname(const QString &surname)
{
    m_sSurname = surname;
}

void Person::setName(const QString &name)
{
    m_sName = name;
}

void Person::setPatronymic(const QString &patronymic)
{
    m_sPatronymic = patronymic;
}

void Person::setBirthday(const QDate &birtday)
{
    m_dateBirthday = birtday;
}

void Person::setAddress(const QString &address)
{
    m_sAddress = address;
}

void Person::setPhone(const QString &phone)
{
    m_sPhone = phone;
}

void Person::setId(int id)
{
    m_iId = id;
}

void Person::setDocument(const Document *document)
{
    m_pDocument = document->clone();
}

void Person::setRepresentative(Person *representative)
{
    m_pRepresentative = representative;
}

Person *&Person::clone() const
{
    Person *person = new Person;
    person->setSurname(m_sSurname);
    person->setName(m_sName);
    person->setPatronymic(m_sPatronymic);
    person->setAddress(m_sAddress);
    person->setPhone(m_sPhone);
    person->setBirthday(m_dateBirthday);
    person->setDocument(m_pDocument);
    Person *representative = nullptr;
    if (m_pRepresentative != nullptr)
    {
        representative = new Person;
        representative->setSurname(m_pRepresentative->m_sSurname);
        representative->setName(m_pRepresentative->m_sName);
        representative->setPatronymic(m_pRepresentative->m_sPatronymic);
        representative->setAddress(m_pRepresentative->m_sAddress);
        representative->setPhone(m_pRepresentative->m_sPhone);
        representative->setBirthday(m_pRepresentative->m_dateBirthday);
        representative->setDocument(m_pRepresentative->m_pDocument);
    }
    person->setRepresentative(representative);

    return person;
}
