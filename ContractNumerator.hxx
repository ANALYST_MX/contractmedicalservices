#pragma once

#include <QObject>
#include <QDate>

class ContractNumerator : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QDate startDate READ getStartDate WRITE setStartDate NOTIFY startDateChanged)
    Q_PROPERTY(int fromNumber READ getFromNumber WRITE setFromNumber NOTIFY fromNumberChanged)
    Q_PROPERTY(int toNumber READ getToNumber WRITE setToNumber NOTIFY toNumberChanged)
    Q_PROPERTY(QString serial READ getSerial WRITE setSerial NOTIFY serialChanged)
    Q_PROPERTY(int numberLength READ getNumberLength WRITE setNumberLength NOTIFY numberLengthChanged)

public:
    explicit ContractNumerator(QObject * = nullptr);
    QDate getStartDate() const;
    int getFromNumber() const;
    int getToNumber() const;
    QString getSerial() const;
    int getNumberLength() const;

signals:
    void startDateChanged(QDate);
    void fromNumberChanged(int);
    void toNumberChanged(int);
    void serialChanged(const QString &);
    void numberLengthChanged(int);

public slots:
    void setStartDate(QDate);
    void setFromNumber(int);
    void setToNumber(int);
    void setSerial(const QString &);
    void setNumberLength(int);

private:
    QDate m_dateStartDate;
    int m_iFromNumber;
    int m_iToNumber;
    QString m_sSerial;
    int m_iNumberLength;
};
