#pragma once

#include <QDialog>
#include <QtWidgets>

#include "DBConnection.hxx"
#include "ContractNumeratorCommands.hxx"
#include "ContractNumerator.hxx"

class SettingNumeratorWindow : public QDialog
{
    Q_OBJECT
public:
    explicit SettingNumeratorWindow(QWidget * = nullptr);
    void view();
    void closeEvent(QCloseEvent *);
    void showEvent(QShowEvent *);

signals:

public slots:
    void saveSettings();

private:
    QLineEdit *m_ptxtContractSerial;
    QSpinBox *m_pspbContractNumberLength;
    QLineEdit *m_ptxtStartRange;
    QLineEdit *m_ptxtEndRange;
    QDateEdit *m_pdateStartDate;
    DBConnection *m_pDatebase;
    void readSettings();
};
