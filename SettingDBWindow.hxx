#pragma once

#include <QDialog>
#include <QtWidgets>
#include <QSqlDatabase>

#include "DBConnection.hxx"
#include "Singleton.hxx"
#include "Configure.hxx"

class SettingDBWindow : public QDialog
{
    Q_OBJECT
public:
    explicit SettingDBWindow(QWidget * = nullptr);
    void view();
    void closeEvent(QCloseEvent *);
    void showEvent(QShowEvent *);

public slots:
    void saveSettings();

private:
    Configure *m_pconf;
    QComboBox *m_pcboDriverName;
    QLineEdit *m_ptxtDatabaseName;
    QLineEdit *m_ptxtUserName;
    QLineEdit *m_ptxtPassword;
    QLineEdit *m_ptxtHostName;
    QSpinBox *m_pspbPort;
    void readSettings();
};
