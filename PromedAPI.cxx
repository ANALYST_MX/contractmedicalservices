#include "PromedAPI.hxx"

PromedAPI::PromedAPI(QObject *parent)
    : QObject(parent)
{
}

bool PromedAPI::checkNetworkError(QNetworkReply* reply)
{
    return reply->error() == QNetworkReply::NoError;
}
