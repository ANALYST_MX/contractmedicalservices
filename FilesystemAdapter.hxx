#pragma once

#include <QObject>
#include <QUrl>
#include <QFileInfo>

class FilesystemAdapter : public QObject
{
    Q_OBJECT
public:
    explicit FilesystemAdapter(QObject * = nullptr);
    Q_INVOKABLE static bool fileExists(const QString &);
    Q_INVOKABLE static bool copyFile(const QString &, const QString &);
    Q_INVOKABLE static bool deleteFile(const QString &);
    Q_INVOKABLE static bool writeToFile(const QString &, const QString &);
    Q_INVOKABLE static QString readFromFile(const QString &);


signals:

public slots:
};
