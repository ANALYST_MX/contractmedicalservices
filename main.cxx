#include "main.hxx"

int main(int argc, char *argv[])
{
    App app(argc, argv, "RCH named after G.G.Kuvatov", "PatientInfo");
    MainWindow wgt;
    wgt.show();

    return app.exec();
}
