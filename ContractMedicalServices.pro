#-------------------------------------------------
#
# Project created by QtCreator 2015-10-21T10:54:56
#
#-------------------------------------------------

QT       += core
QT       += gui
QT       += sql
QT       += network
QT       += printsupport
QT       += webkitwidgets
QT       += widgets
QT       += axcontainer

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ContractMedicalServices
TEMPLATE = app


SOURCES += main.cxx \
    MainWindow.cxx \
    GroupBoxDepartment.cxx \
    AuthPromed.cxx \
    App.cxx \
    Configure.cxx \
    Person.cxx \
    PersonPromed.cxx \
    PromedJson.cxx \
    Session.cxx \
    PortableSleep.cxx \
    SettingAuthWindow.cxx \
    SettingDBWindow.cxx \
    SettingPrintWindow.cxx \
    DBConnection.cxx \
    SqlException.cxx \
    SqlDatabaseException.cxx \
    SqlModel.cxx \
    LogicalFaultException.cxx \
    SqlFailedException.cxx \
    SqlDataNotFoundException.cxx \
    MedicalServiceEntry.cxx \
    MedicalServiceList.cxx \
    IndexOutOfBound.cxx \
    CartMedicalServiceList.cxx \
    MedicalServiceModel.cxx \
    Contract.cxx \
    UrlUtil.cxx \
    Document.cxx \
    PromedAPI.cxx \
    SettingNumeratorWindow.cxx \
    ContractNumerator.cxx \
    ContractNumeratorCommands.cxx \
    WordDocument.cxx \
    FileDocument.cxx \
    FilesystemAdapter.cxx \
    Translate.cxx

HEADERS  += \
    MainWindow.hxx \
    main.hxx \
    GroupBoxDepartment.hxx \
    EnumHelpers.hxx \
    AuthPromed.hxx \
    App.hxx \
    Configure.hxx \
    Singleton.hxx \
    CallOnce.hxx \
    Person.hxx \
    PersonPromed.hxx \
    PromedJson.hxx \
    Session.hxx \
    PortableSleep.hxx \
    SettingAuthWindow.hxx \
    SettingDBWindow.hxx \
    SettingPrintWindow.hxx \
    DBConnection.hxx \
    SqlException.hxx \
    SqlDatabaseException.hxx \
    SqlModel.hxx \
    LogicalFaultException.hxx \
    DBQueries.hxx \
    SqlFailedException.hxx \
    SqlDataNotFoundException.hxx \
    MedicalServiceEntry.hxx \
    MedicalServiceList.hxx \
    IndexOutOfBound.hxx \
    CartMedicalServiceList.hxx \
    MedicalServiceModel.hxx \
    Contract.hxx \
    UrlUtil.hxx \
    Document.hxx \
    PromedAPI.hxx \
    SettingNumeratorWindow.hxx \
    ContractNumerator.hxx \
    ContractNumeratorCommands.hxx \
    WordDocument.hxx \
    FileDocument.hxx \
    FilesystemAdapter.hxx \
    Translate.hxx

CONFIG += c++11
QMAKE_CXXFLAGS += -Wno-return-local-addr
