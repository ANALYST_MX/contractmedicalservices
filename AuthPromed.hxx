#pragma once

#include <QString>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkCookieJar>
#include <QNetworkReply>
#include <QUrl>
#include <QUrlQuery>
#include <QRegExp>
#include <QMessageBox>
#include <QJsonDocument>
#include <QJsonObject>

#include "Singleton.hxx"
#include "Configure.hxx"
#include "AuthPromed.hxx"
#include "PromedAPI.hxx"

class AuthPromed : public QObject
{
    Q_OBJECT

public:
    explicit AuthPromed(QObject * = nullptr);
    QString getAccessToken() const;
    bool isAuth() const;

public slots:
    void readyStatusReply();
    void portalPage();

signals:
    void logOn();
    void successAuth();
    void failedAuth();
    void error();

private:
    Configure *m_pConf;
    QString m_sAccessToken;
    bool m_bAuthFlag;
    void readyCookieReply(const QNetworkReply *);
};
