#include "PersonPromed.hxx"

PersonPromed::PersonPromed(AuthPromed *auth, QObject *parent)
    : QObject(parent), m_pAuth(auth)
{
    m_pconf = Singleton<Configure>::instance();
    m_pPerson = new Person(this);
    m_pDocument = new Document(this);

    connect(this, SIGNAL(searchPersonId()), SLOT(getPersonSearchGrid()));
    connect(this, SIGNAL(searchPersonInfo()), SLOT(getPersonEditWindow()));
    connect(this, SIGNAL(searchOrgDepName()), SLOT(getOrgDepList()));
}

void PersonPromed::findByFIOAndBirthday(const QString &surname, const QString &name, const QString &patronymic, const QDate &birthday)
{
    m_pPerson->setSurname(surname);
    m_pPerson->setName(name);
    m_pPerson->setPatronymic(patronymic);
    m_pPerson->setBirthday(birthday);

    emit searchPersonId();
}

Person *PersonPromed::getSearchResult()
{
    return m_pPerson->clone();
}

void PersonPromed::getPersonSearchGrid()
{
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    QUrl apiUrl = QString("%1").arg(m_pconf->getBaseURLAuth());
    apiUrl.setPath("/");
    QUrlQuery query;
    query.addQueryItem("c", "Person");
    query.addQueryItem("m", "getPersonSearchGrid");
    apiUrl.setQuery(query);
    QNetworkRequest request(apiUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
    request.setRawHeader("Cookie", m_pAuth->getAccessToken().toUtf8());

    QUrlQuery postData;
    postData.addQueryItem("PersonSurName_SurName", m_pPerson->getSurname().toUpper().toUtf8());
    postData.addQueryItem("PersonFirName_FirName", m_pPerson->getName().toUpper().toUtf8());
    postData.addQueryItem("PersonSecName_SecName", m_pPerson->getPatronymic().toUpper().toUtf8());
    postData.addQueryItem("PersonBirthDay_BirthDay", m_pPerson->getBirthday().toString("dd.MM.yyyy"));
    postData.addQueryItem("limit", QString::number(COUNT));
    QNetworkReply *reply = manager->post(request, postData.toString(QUrl::FullyEncoded).toUtf8());
    connect(reply, SIGNAL(finished()), SLOT(readyPersonIdFromServer()));
}

void PersonPromed::readyPersonIdFromServer()
{
    QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender());
    if(!PromedAPI::checkNetworkError(reply))
    {
        emit error();
    }
    else
    {
        reply->deleteLater();
        bool ok;
        QString json = reply->readAll();
        QVariantMap result = PromedJson::parse(json, ok).toMap();
        if(!ok)
        {
            emit error();
        }
        else
        {
            if (result["totalCount"].toInt() == COUNT)
            {
                m_pPerson->setId(result["data"].toList()[0].toMap()["Person_id"].toInt());

                emit searchPersonInfo();
            }
            else
            {
                emit failedPersonNotFound();
            }
        }
    }
}

void PersonPromed::getPersonEditWindow()
{
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    QUrl apiUrl = QString("%1").arg(m_pconf->getBaseURLAuth());
    apiUrl.setPath("/");
    QUrlQuery query;
    query.addQueryItem("c", "Person");
    query.addQueryItem("m", "getPersonEditWindow");
    apiUrl.setQuery(query);
    QNetworkRequest request(apiUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
    request.setRawHeader("Cookie", m_pAuth->getAccessToken().toUtf8());
    QUrlQuery postData;
    postData.addQueryItem("person_id", QString::number(m_pPerson->getId()).toUtf8());
    QNetworkReply *reply = manager->post(request, postData.toString(QUrl::FullyEncoded).toUtf8());
    connect(reply, SIGNAL(finished()), SLOT(readyPersonInfoFromServer()));
}

void PersonPromed::readyPersonInfoFromServer()
{
    QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender());
    if(!PromedAPI::checkNetworkError(reply))
    {
        emit error();
    }
    else
    {
        reply->deleteLater();
        bool success;
        QString json = reply->readAll();
        QVariantMap result = PromedJson::parse(json, success).toList()[0].toMap();
        if(!success)
        {
            emit error();
        }
        else
        {
            if (!result.empty())
            {
                m_pPerson->setSurname(result["Person_SurName"].toString());
                m_pPerson->setName(result["Person_FirName"].toString());
                m_pPerson->setPatronymic(result["Person_SecName"].toString());
                m_pPerson->setPhone(result["PersonPhone_Phone"].toString());
                m_pPerson->setAddress(result["PAddress_AddressText"].toString());

                m_pDocument->setSerial(result["Document_Ser"].toString());
                m_pDocument->setNumber(result["Document_Num"].toString());
                m_pDocument->setType(result["DocumentType_id"].toInt() + 1);
                m_pDocument->setDateOfIssue(QDate::fromString(result["Document_begDate"].toString(), "dd.MM.yyyy"));
                m_pDocument->setAuthorityId(result["OrgDep_id"].toInt());
                m_pPerson->setDocument(m_pDocument);

                emit searchOrgDepName();
            }
            else
            {
                emit failedPersonNotFound();
            }
        }
    }
}

void PersonPromed::getOrgDepList()
{
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    QUrl apiUrl = QString("%1").arg(m_pconf->getBaseURLAuth());
    apiUrl.setPath("/");
    QUrlQuery query;
    query.addQueryItem("c", "Utils");
    query.addQueryItem("m", "GetObjectList");
    apiUrl.setQuery(query);
    QNetworkRequest request(apiUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
    request.setRawHeader("Cookie", m_pAuth->getAccessToken().toUtf8());
    QUrlQuery postData;
    postData.addQueryItem("Object", "OrgDep");
    postData.addQueryItem("OrgDep_id", QString::number(m_pDocument->getAuthorityId()).toUtf8());
    postData.addQueryItem("OrgDep_Name", "");
    QNetworkReply *reply = manager->post(request, postData.toString(QUrl::FullyEncoded).toUtf8());
    connect(reply, SIGNAL(finished()), SLOT(readyOrgDepNameFromServer()));
}


void PersonPromed::readyOrgDepNameFromServer()
{
    QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender());
    if(!PromedAPI::checkNetworkError(reply))
    {
        emit error();
    }
    else
    {
        reply->deleteLater();
        bool success;
        QString json = reply->readAll();
        QVariantMap result = PromedJson::parse(json, success).toList()[0].toMap();
        if(!success)
        {
            emit error();
        }
        else
        {
            if (!result.empty())
            {
                m_pDocument->setAuthority(result["OrgDep_Name"].toString());
                m_pPerson->setDocument(m_pDocument);

                emit ok();
            }
            else
            {
                emit failedOrgDepNotFound();
            }
        }
    }
}
