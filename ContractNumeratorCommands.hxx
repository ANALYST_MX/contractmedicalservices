#pragma once

#include <QString>
#include <QSqlDatabase>
#include <QSqlQuery>

#include "ContractNumerator.hxx"
#include "DBQueries.hxx"
#include "SqlFailedException.hxx"
#include "SqlDatabaseException.hxx"
#include "SqlDataNotFoundException.hxx"

class ContractNumeratorCommands : public QObject
{
    Q_OBJECT
public:
    explicit ContractNumeratorCommands(QSqlDatabase *, QObject * = nullptr);

signals:

public slots:
    bool insert(ContractNumerator *);
    bool update(ContractNumerator *);
    ContractNumerator *select(QDate);
    ContractNumerator *find(QDate);
    QString getLastError();
    bool updateStartIntervalValue(int);
    int incrementNumber();

private:
    QString m_sLastError;
    QSqlDatabase *m_pDatabase;
};
