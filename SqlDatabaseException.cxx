#include "SqlDatabaseException.hxx"

SqlDatabaseException::SqlDatabaseException(const char *what)
    : SqlException(what)
{
}
