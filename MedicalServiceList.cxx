#include "MedicalServiceList.hxx"

MedicalServiceList::MedicalServiceList(QObject *parent)
    : QAbstractListModel(parent)
{
    roles[IdRole] = "id";
    roles[CodeRole] = "code";
    roles[NameRole] = "name";
    roles[PriceRole] = "price";
}

QHash<int, QByteArray> MedicalServiceList::roleNames() const
{
    return roles;
}

QVariant MedicalServiceList::data(const QModelIndex &index, int role) const
{
    if (index.row() < 0 || index.row() >= m_medicalServices.count())
    {
        return QVariant();
    }
    MedicalServiceModel * const item = m_medicalServices.at(index.row());
    switch (role) {
    case IdRole:
        return item->key();
    case CodeRole:
        return item->getCode();
    case NameRole:
        return item->getName();
    case PriceRole:
        return item->getPrice();
    }
    return QVariant();
}

int MedicalServiceList::rowCount(const QModelIndex &) const
{
    return m_medicalServices.count();
}

MedicalServiceModel *MedicalServiceList::get(int index)
{
    if (index < 0 || index >= m_medicalServices.count())
    {
        throw IndexOutOfBound(index);
    }

    return m_medicalServices.at(index);
}

void MedicalServiceList::findMedicalServices(const QString &filter)
{
    m_medicalServices.clear();
    QSqlQuery query(querySelectMedicalServicesByCodeOrName.arg(filter));

    while(query.next())
    {
        MedicalServiceModel *medicalService = new MedicalServiceModel(this);
        medicalService->setPrimaryKey(query.value(0).toInt());
        medicalService->setCode(query.value(1).toString());
        medicalService->setName(query.value(2).toString());
        medicalService->setPrice(query.value(3).toDouble());
        m_medicalServices.append(medicalService);
    }
}
