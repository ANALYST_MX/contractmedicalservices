#include "MedicalServiceEntry.hxx"

MedicalServiceEntry::MedicalServiceEntry(QObject *parent)
    : QObject(parent)
{
}

void MedicalServiceEntry::setCode(const QString &code)
{
    m_sCode = code;
}

void MedicalServiceEntry::setName(const QString &name)
{
    m_sName = name;
}

void MedicalServiceEntry::setPrice(const double price)
{
    m_dPrice = price;
}

QString MedicalServiceEntry::getCode() const
{
    return m_sCode;
}

QString MedicalServiceEntry::getName() const
{
    return m_sName;
}

double MedicalServiceEntry::getPrice() const
{
    return m_dPrice;
}

int MedicalServiceEntry::getCount() const
{
    return m_iCount;
}

void MedicalServiceEntry::setCount(const int count)
{
    m_iCount = count;
}

void MedicalServiceEntry::setDateExpires(const QDate &dateExpires)
{
    m_dateExpires = dateExpires;
}

QDate MedicalServiceEntry::getDateExpires() const
{
    return m_dateExpires;
}

MedicalServiceEntry *&MedicalServiceEntry::clone() const
{
    MedicalServiceEntry *usluga = new MedicalServiceEntry;
    usluga->setCode(m_sCode);
    usluga->setName(m_sName);
    usluga->setPrice(m_dPrice);
    usluga->setCount(m_iCount);
    usluga->setDateExpires(m_dateExpires);
    return usluga;
}
