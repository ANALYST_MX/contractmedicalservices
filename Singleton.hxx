#pragma once

#include <QtGlobal>
#include <QScopedPointer>
#include "CallOnce.hxx"

template <class T>
class Singleton
{
public:
    static T* instance()
    {
        qCallOnce(init, flag);
        return tptr.data();
    }

    static void init()
    {
        tptr.reset(new T);
    }

private:
    Singleton() {}
    ~Singleton() {}
    Q_DISABLE_COPY(Singleton)

    static QScopedPointer<T> tptr;
    static QBasicAtomicInt flag;
};

template<class T> QScopedPointer<T> Singleton<T>::tptr(0);
template<class T> QBasicAtomicInt Singleton<T>::flag = Q_BASIC_ATOMIC_INITIALIZER(CallOnce::CO_Request);
