#pragma once

#include "SqlException.hxx"

class SqlFailedException : public SqlException
{
public:
    SqlFailedException(const char *);
};
