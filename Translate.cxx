#include "Translate.hxx"

std::map<Translate::LANG, std::array<std::string, SIZE_TRANSLIT>> Translate::translit
{
    {Translate::LANG::ENG, {"q","w","e","r","t","y","u","i","o","p","[","]","a","s","d","f","g","h","j","k","l",";","'","z","x","c","v","b","n","m",",",".","/","Q","W","E","R","T","Y","U","I","O","P","[","]","A","S","D","F","G","H","J","K","L",";","'","Z","X","C","V","B","N","M",",",".","/","&"}},
    {Translate::LANG::RUS, {"й","ц","у","к","е","н","г","ш","щ","з","х","ъ","ф","ы","в","а","п","р","о","л","д","ж","э","я","ч","с","м","и","т","ь","б","ю",".","Й","Ц","У","К","Е","Н","Г","Ш","Щ","З","Х","Ъ","Ф","Ы","В","А","П","Р","О","Л","Д","Ж","Э","Я","Ч","С","М","И","Т","Ь","Б","Ю",".","?"}}
};

Translate::Translate(QObject *parent)
    : QObject(parent)
{
}

std::string Translate::translateToRus(std::string str)
{
    if (str.empty()) return str;
    std::string translate = str;
    for (size_t i = 0; i < SIZE_TRANSLIT; i++)
    {
        replaceAll(translate, translit[LANG::ENG][i], translit[LANG::RUS][i]);
    }
    return translate;
}

QString Translate::translateToRus(QString str)
{
    return QString::fromStdString(translateToRus(str.toStdString()));
}

std::string Translate::translateToEng(std::string str)
{
    if (str.empty()) return str;
    std::string translate = str;
    for (size_t i = 0; i < SIZE_TRANSLIT; ++i)
    {
        replaceAll(translate, translit[LANG::RUS][i], translit[LANG::ENG][i]);
    }
    return translate;
}

QString Translate::translateToEng(QString str)
{
    return QString::fromStdString(translateToEng(str.toStdString()));
}

void Translate::replaceAll(std::string &str, const std::string &from, const std::string &to)
{
    if (from.empty()) return;
    size_t pos = 0;
    while((pos = str.find(from, pos)) != std::string::npos)
    {
        str.replace(pos, from.length(), to);
        pos += to.length();
    }
}

Translate::LANG Translate::getCurentLang(const std::string &str)
{
    Translate::LANG thislang = Translate::LANG::ENG;
    const char *cstr = str.c_str();
    static std::array<std::string, 66> russtr = {"й","ц","у","к","е","н","г","ш","щ","з","х","ъ","ф","ы","в","а","п","р","о","л","д","ж","э","я","ч","с","м","и","т","ь","б","ю","ё","Й","Ц","У","К","Е","Н","Г","Ш","Щ","З","Х","Ъ","Ф","Ы","В","А","П","Р","О","Л","Д","Ж","Э","Я","Ч","С","М","И","Т","Ь","Б","Ю","Ё"};
    for (size_t i = 0; i < str.length(); ++i)
    {
        for (size_t a = 0; a < 66; ++a)
        {
            if (russtr[a].compare(new char (cstr[i])) == true)
            {
                thislang = Translate::LANG::RUS;
            }
        }
    }
    return thislang;
}

Translate::LANG Translate::getCurentLang(QString str)
{
    return getCurentLang(str.toStdString());
}

#undef SIZE_TRANSLIT
