#pragma once

#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrl>
#include <QUrlQuery>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>

#include "Singleton.hxx"
#include "Configure.hxx"
#include "Person.hxx"
#include "PromedJson.hxx"
#include "Document.hxx"
#include "PromedAPI.hxx"

class PersonPromed : public QObject
{
    Q_OBJECT
public:
    PersonPromed(AuthPromed *, QObject * = nullptr);
    void findByFIOAndBirthday(const QString &, const QString &, const QString &, const QDate &);
    Person *getSearchResult();

public slots:
    void getPersonSearchGrid();
    void getPersonEditWindow();
    void getOrgDepList();
    void readyPersonIdFromServer();
    void readyPersonInfoFromServer();
    void readyOrgDepNameFromServer();

signals:
    void searchPersonId();
    void searchPersonInfo();
    void searchOrgDepName();
    void error();
    void ok();
    void failedPersonNotFound();
    void failedOrgDepNotFound();

private:
    AuthPromed *m_pAuth;
    Person *m_pPerson;
    Document *m_pDocument;
    const int COUNT = 1;
    Configure *m_pconf;
};
