#include "WordDocument.hxx"

WordDocument::WordDocument(const QString &path, QObject *parent)
    : FileDocument(path, parent), m_pWordDocuments(nullptr), m_pWordDocument(nullptr)
{
    m_pWordApplication = new QAxObject(parent);
}

WordDocument::~WordDocument()
{
    delete m_pWordDocument;
    delete m_pWordDocuments;
    delete m_pWordApplication;
    m_pWordDocument = nullptr;
    m_pWordDocuments = nullptr;
    m_pWordApplication = nullptr;
}

void WordDocument::open()
{
    if (!isOpen())
    {
        m_pWordApplication->setControl("Word.Application");
        m_pWordApplication->setProperty("DisplayAlerts", false);
        QList<QVariant> params;
        params << getPath() << false << false << false << "" << "" << false;
        m_pWordDocuments = m_pWordApplication->querySubObject("Documents");
        m_pWordDocuments->querySubObject("Open(const QVariant&,const QVariant&,"
                                         "const QVariant&,const QVariant&,"
                                         "const QVariant&,const QVariant&,"
                                         "const QVariant&)", params);
        m_pWordDocument = m_pWordApplication->querySubObject("ActiveDocument");
    }
}

bool WordDocument::isOpen() const
{
    return m_pWordDocument != nullptr;
}

QList<QVariant> WordDocument::retrieveDefaultParam(bool replaceAll)
{
    QString wdReplace = "1";  // wdReplaceOne
    if (replaceAll)
    {
        wdReplace = "2";      // wdReplaceAll
    }
    QList< QVariant > params;
    params << QVariant("");                   // FindText
    params << QVariant("0");                  // MatchCase
    params << QVariant("0");                  // MatchWholeWord
    params << QVariant("0");                  // MatchWildcards
    params << QVariant("0");                  // MatchSoundsLike
    params << QVariant("0");                  // MatchAllWordForms
    params << QVariant(true);                 // Forward
    params << QVariant("0");                  // Wrap
    params << QVariant("0");                  // Format
    params << QVariant("");                   // ReplaceWith
    params << QVariant(wdReplace);            // Replace
    params << QVariant("0");                  // MatchKashida
    params << QVariant("0");                  // MatchDiacritics
    params << QVariant("0");                  // MatchAlefHamza
    params << QVariant("0");                  // MatchControl
    return params;
}

void WordDocument::setVisible(bool visible)
{
    m_pWordApplication->setProperty("Visible", visible);
}

void WordDocument::substitution(const QString &findText, const QString &replaceWith, bool replaceAll)
{
    std::unique_ptr< QAxObject > next((QAxObject *)m_pWordDocument->querySubObject("Content"));
    std::unique_ptr< QAxObject > find((QAxObject *)next->querySubObject("Find"));
    if (find)
    {
        find->dynamicCall("ClearFormatting()");
        QList<QVariant> params = retrieveDefaultParam(replaceAll);
        params[0] = QVariant(findText);
        params[9] = QVariant(replaceWith);
        find->dynamicCall( "Execute(const QVariant&, const QVariant&, const QVariant&, "
                           "const QVariant&, const QVariant&, const QVariant&, "
                           "const QVariant&, const QVariant&, const QVariant&, "
                           "const QVariant&, const QVariant&, const QVariant&, "
                           "const QVariant&, const QVariant&, const QVariant&)",
                           params);
    }
}

void WordDocument::saveAs(const QString &newPath)
{
    if (m_pWordDocument)
    {
        m_pWordDocument->dynamicCall("SaveAs(const QString&)", newPath);
    }
}

void WordDocument::save()
{
    if (m_pWordDocument)
    {
        m_pWordDocument->dynamicCall("Save()");
    }
}

void WordDocument::close()
{
    if (m_pWordDocument)
    {
        m_pWordDocument->dynamicCall("Close(boolean)", false);
        m_pWordDocument->clear();
    }
    if (m_pWordDocuments)
    {
        m_pWordDocument->clear();
    }
    if (m_pWordApplication)
    {
        m_pWordApplication->dynamicCall("Quit(void)");
        m_pWordApplication->clear();
    }
}

void WordDocument::print() const
{
    if (m_pWordApplication && m_pWordDocument)
    {
        QString lastPrinter = m_pWordApplication->property("ActivePrinter").toString();
        m_pWordDocument->dynamicCall("PrintOut(boolean)", false);
    }
}
