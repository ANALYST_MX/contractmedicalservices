#include "SettingNumeratorWindow.hxx"

SettingNumeratorWindow::SettingNumeratorWindow(QWidget *parent)
    : QDialog(parent)
{
    m_pDatebase = DBConnection::instance();
    view();
}

void SettingNumeratorWindow::view()
{
    QFormLayout *layout = new QFormLayout;
    layout->setMargin(5);
    layout->setSpacing(5);

    QLabel *lblContractSerial = new QLabel("Серия:");
    m_ptxtContractSerial = new QLineEdit;
    layout->addRow(lblContractSerial, m_ptxtContractSerial);

    QLabel *lblContractNumberLength = new QLabel("Длина номера:");
    m_pspbContractNumberLength = new QSpinBox;
    layout->addRow(lblContractNumberLength, m_pspbContractNumberLength);

    QLabel *lblStartRange = new QLabel("Начало:");
    m_ptxtStartRange = new QLineEdit;
    layout->addRow(lblStartRange, m_ptxtStartRange);

    QLabel *lblEndRange = new QLabel("Конец:");
    m_ptxtEndRange = new QLineEdit;
    layout->addRow(lblEndRange, m_ptxtEndRange);

    QLabel *lblStartDateRange = new QLabel("Дата:");
    m_pdateStartDate = new QDateEdit;
    layout->addRow(lblStartDateRange, m_pdateStartDate);

    QPushButton *pcmdCancel = new QPushButton("Cancel");
    QPushButton *pcmdOk = new QPushButton("Ok");
    connect(pcmdCancel, SIGNAL(clicked()), SLOT(close()));
    connect(pcmdOk, SIGNAL(clicked()), SLOT(saveSettings()));
    layout->addRow(pcmdCancel, pcmdOk);

    setLayout(layout);
}

void SettingNumeratorWindow::closeEvent(QCloseEvent* evt)
{
    this->hide();
    evt->ignore();
}

void SettingNumeratorWindow::showEvent(QShowEvent *evt)
{
    readSettings();
    QWidget::showEvent(evt);
}

void SettingNumeratorWindow::saveSettings()
{
    ContractNumeratorCommands commands(m_pDatebase->getDatabase());
    ContractNumerator *interval = new ContractNumerator;
    interval->setFromNumber(m_ptxtStartRange->text().toInt());
    interval->setToNumber(m_ptxtEndRange->text().toInt());
    interval->setStartDate(m_pdateStartDate->date());
    interval->setSerial(m_ptxtContractSerial->text());
    interval->setNumberLength(m_pspbContractNumberLength->value());
    if (nullptr != commands.find(interval->getStartDate()))
    {
        commands.update(interval);
    }
    else
    {
        commands.insert(interval);
    }

    delete interval;
    close();
}

void SettingNumeratorWindow::readSettings()
{
    ContractNumeratorCommands commands(m_pDatebase->getDatabase());
    ContractNumerator *interval = nullptr;
    interval = commands.select(QDate::currentDate());
    if(nullptr == interval)
    {
        interval = new ContractNumerator;
        interval->setStartDate(QDate::currentDate());
        interval->setFromNumber(0);
        interval->setToNumber(0);
        interval->setSerial("");
        interval->setNumberLength(0);
    }
    m_ptxtContractSerial->setText(interval->getSerial());
    m_pspbContractNumberLength->setValue(interval->getNumberLength());
    m_ptxtStartRange->setText(QString::number(interval->getFromNumber()));
    m_ptxtEndRange->setText(QString::number(interval->getToNumber()));
    m_pdateStartDate->setDate(interval->getStartDate());

    delete interval;
}
