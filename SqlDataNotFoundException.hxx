#pragma once

#include "SqlException.hxx"

class SqlDataNotFoundException : public SqlException
{
public:
    SqlDataNotFoundException(const char *);
};
