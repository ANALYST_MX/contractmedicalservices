#include "CartMedicalServiceList.hxx"

CartMedicalServiceList::CartMedicalServiceList(QObject *parent)
    : QObject(parent)
{

}

QHash<QString, MedicalServiceEntry *>::iterator CartMedicalServiceList::begin()
{
    return m_medicalServices.begin();
}

QHash<QString, MedicalServiceEntry *>::iterator CartMedicalServiceList::end()
{
    return m_medicalServices.end();
}

int CartMedicalServiceList::size() const
{
    return m_medicalServices.size();
}

void CartMedicalServiceList::clear()
{
    m_medicalServices.clear();
}

bool CartMedicalServiceList::contains(const QString &key) const
{
    return m_medicalServices.contains(key);
}

void CartMedicalServiceList::set(const QString &key, MedicalServiceEntry *value)
{
    m_medicalServices[key] = value;
}

MedicalServiceEntry *CartMedicalServiceList::get(const QString &key)
{
    return m_medicalServices[key]->clone();
}

void CartMedicalServiceList::remove(const QString &key)
{
    m_medicalServices.remove(key);
}
