#pragma once

#include <QObject>

#include "MedicalServiceEntry.hxx"
#include "SqlModel.hxx"
#include "DBQueries.hxx"
#include "LogicalFaultException.hxx"
#include "SqlFailedException.hxx"
#include "SqlDatabaseException.hxx"
#include "SqlDataNotFoundException.hxx"

class MedicalServiceModel : public MedicalServiceEntry, public SqlModel
{
    Q_OBJECT
public:
    explicit MedicalServiceModel(QObject * = nullptr);
    void save() throw(SqlException);
    void construct() throw(SqlException, LogicalFaultException);
    void erase() throw(SqlException, LogicalFaultException);
};
