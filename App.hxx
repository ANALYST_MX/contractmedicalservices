#pragma once

#include <QApplication>
#include <QSettings>

class App : public QApplication
{
    Q_OBJECT
public:
    App(int &, char **, const QString &, const QString &);
    static App *theApp();
    QSettings *getSettings();

private:
    QSettings *m_pSettings;
};
