#include "MedicalServiceModel.hxx"
#include "DBConnection.hxx"

MedicalServiceModel::MedicalServiceModel(QObject *parent)
    : MedicalServiceEntry(parent), SqlModel(DBConnection::instance()->getDatabase(), "medical_services")
{

}

void MedicalServiceModel::save() throw(SqlException)
{
    if(db()->isOpen())
    {
        if(!key())
        {
            QSqlQuery query;
            query.prepare(queryInsertMedicalService);
            query.bindValue(":code", getCode());
            query.bindValue(":name", getName());
            query.bindValue(":price", getPrice());
            if(!query.exec())
            {
                throw SqlFailedException("Inserting MedicalService failed");
            }
            else
            {
                setPrimaryKey(query.lastInsertId().toUInt());
            }
        }
        else
        {
            QSqlQuery query;
            query.prepare(queryUpdateMedicalService);
            query.bindValue(":id", key());
            query.bindValue(":code", getCode());
            query.bindValue(":name", getName());
            query.bindValue(":price", getPrice());
            if(!query.exec())
            {
                throw SqlFailedException(query.lastError().text().toStdString().c_str());
            }
        }
    }
    else
    {
        throw SqlDatabaseException("Database is not open");
    }
}

void MedicalServiceModel::construct() throw(SqlException, LogicalFaultException)
{
    if(!key())
    {
        throw LogicalFaultException("Key has not been set before construction");
    }
    if(db()->isOpen())
    {
        QSqlQuery query(querySelectMedicalServiceById.arg(key()));

        if(query.next())
        {
            setCode(query.value(1).toString());
            setName(query.value(2).toString());
            setPrice(query.value(3).toDouble());
        }
        else
        {
            throw SqlDataNotFoundException("Data has not been found");
        }
    }
    else
    {
        throw SqlDatabaseException("Database is not open");
    }
}

void MedicalServiceModel::erase() throw(SqlException, LogicalFaultException)
{
    if(!key())
    {
        throw LogicalFaultException("Key has not been set before construction");
    }
    if(db()->isOpen())
    {
        QSqlQuery query(queryDeleteMedicalServiceById.arg(key()));
        if(query.lastError().type() != QSqlError::NoError)
        {
            throw SqlDataNotFoundException("Data has not been found");
        }
    }
    else
    {
        throw SqlDatabaseException("Database is not open");
    }
}
