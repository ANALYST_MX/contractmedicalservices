#include "AuthPromed.hxx"

AuthPromed::AuthPromed(QObject *parent)
    : QObject(parent)
{
    m_pConf = Singleton<Configure>::instance();

    connect(this, SIGNAL(logOn()), SLOT(portalPage()));

    emit logOn();
}

void AuthPromed::readyCookieReply(const QNetworkReply *reply)
{
    QRegExp rx("(PHPSESSID=[\\w]+)");
    auto headerList = reply->rawHeaderList();
    for(auto it = headerList.begin(); it != headerList.end(); ++it)
    {
        if (*it == "Set-Cookie" && rx.indexIn(reply->rawHeader(*it)) != -1)
        {
            m_sAccessToken = rx.cap(1);
            break;
        }
    }
}

void AuthPromed::portalPage()
{
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    QUrl apiUrl = QString("%1").arg(m_pConf->getBaseURLAuth());
    apiUrl.setPath("/");
    apiUrl.setQuery("method=Logon");
    QByteArray query = QString("login=%1&psw=%2").arg(m_pConf->getLoginAuth()).arg(m_pConf->getPasswordAuth()).toStdString().c_str();
    QNetworkRequest request(apiUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
    QNetworkReply *reply = manager->post(request, query);
    connect(reply, SIGNAL(finished()), SLOT(readyStatusReply()));
}

void AuthPromed::readyStatusReply()
{
    QNetworkReply *reply = (QNetworkReply*)sender();
    QJsonParseError parseError;
    QJsonDocument doc = QJsonDocument::fromJson(QString(reply->readAll()).toUtf8(), &parseError);
    if (!PromedAPI::checkNetworkError(reply))
    {
        m_bAuthFlag = false;
        emit failedAuth();
    }
    else
    {
        reply->deleteLater();
        QJsonObject status = doc.object();
        if (status["success"].toBool() == true)
        {
            readyCookieReply(reply);
            m_bAuthFlag = true;
            emit successAuth();
        }
        else
        {
            m_bAuthFlag = false;
            emit failedAuth();
        }
    }
}

QString AuthPromed::getAccessToken() const
{
    return m_sAccessToken;
}

bool AuthPromed::isAuth() const
{
    return m_bAuthFlag;
}
