#pragma once

#include <QString>
#include <QDate>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrl>
#include <QJsonDocument>
#include <QJsonObject>

#include "AuthPromed.hxx"
#include "Document.hxx"

class Person : public QObject
{
    Q_OBJECT

public:
    Person(QObject *parent = nullptr);

    int getId() const;
    QString getSurname() const;
    QString getName() const;
    QString getPatronymic() const;
    QDate getBirthday() const;
    QString getAddress() const;
    QString getPhone() const;
    Document *&getDocument() const;
    const Person *getRepresentative();
    void setSurname(const QString &);
    void setName(const QString &);
    void setPatronymic(const QString &);
    void setBirthday(const QDate &);
    void setAddress(const QString &);
    void setId(int);
    void setPhone(const QString &);
    void setDocument(const Document *);
    void setRepresentative(Person *);
    Person *&clone() const;

private slots:

private:
    int m_iId;
    QString m_sSurname;
    QString m_sName;
    QString m_sPatronymic;
    QDate m_dateBirthday;
    QString m_sAddress;
    QString m_sPhone;
    Document *m_pDocument;
    Person *m_pRepresentative;
};
