#include "FilesystemAdapter.hxx"

#include <QDir>
#include <QDebug>

FilesystemAdapter::FilesystemAdapter(QObject *parent)
    : QObject(parent)
{
}

bool FilesystemAdapter::fileExists(const QString &path)
{
    return QFile(path).exists();
}

bool FilesystemAdapter::copyFile(const QString &source, const QString &destination)
{
    return QFile::copy(source, destination);
}

bool FilesystemAdapter::deleteFile(const QString &path)
{
    return QFile::remove(path);
}

bool FilesystemAdapter::writeToFile(const QString &path, const QString &text)
{
    QFile file(path);
    file.open(QFile::WriteOnly | QFile::Truncate | QFile::Text);
    file.write(text.toLocal8Bit());
    bool ret = file.flush();
    file.close();
    return ret;
}

QString FilesystemAdapter::readFromFile(const QString &path)
{
    QFile file(path);
    file.open(QFile::ReadOnly | QFile::Text);
    QString text = QString::fromLocal8Bit(file.readAll());
    file.close();
    return text;
}
