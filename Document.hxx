#pragma once

#include <QObject>
#include <QMap>
#include <QString>
#include <QDate>
#include <QPair>

class Document : public QObject
{
    Q_OBJECT
public:
    explicit Document(QObject * = nullptr);
    QString getSerial() const;
    QString getNumber() const;
    QString getAuthority() const;
    QDate getDateOfIssue() const;
    QDate getDateOfExpiration() const;
    QPair<int, QString> getType() const;
    int getAuthorityId() const;
    void setSerial(const QString &);
    void setNumber(const QString &);
    void setAuthority(const QString &);
    void setDateOfIssue(const QDate &);
    void setDateOfExpiration(const QDate &);
    void setType(int);
    void setAuthorityId(int);
    Document *&clone() const;

public:
    static QMap<int, QString> types();
    static QString type(int);

private:
    int m_iType;
    QString m_sSerial;
    QString m_sNumber;
    QString m_sAuthority;
    int m_iAuthorityId;
    QDate m_dateOfIssue;
    QDate m_dateOfExpiration;

private:
    static const QMap<int, QString> TYPES;
};
