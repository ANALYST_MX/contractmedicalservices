#pragma once

#include <memory>
#include <iomanip>
#include <sstream>
#include <QObject>
#include <QString>
#include <QTextStream>
#include <QTextDocument>
#include <QPrintDialog>
#include <QList>
#include <QPrintPreviewDialog>
#include <QPrinter>
#include <QWebView>
#include <QFile>
#include <QSqlDatabase>
#include <QSqlQuery>

#include "Person.hxx"
#include "MedicalServiceEntry.hxx"
#include "ContractNumerator.hxx"
#include "DBQueries.hxx"
#include "WordDocument.hxx"

class Contract : public QObject
{
    Q_OBJECT
public:
    explicit Contract(QSqlDatabase *, const QString &, QObject * = nullptr);
    ~Contract();
    void setClient(Person *);
    void addUsluga(MedicalServiceEntry *);
    void setCountMedicalServices(int);
    void printPreview(bool = false);
    void setContractNumerator(ContractNumerator *);
    void setDateContract(QDate);
    void setOutputPathDirectory(const QString &);
    void setCurrencyDecimalPlaces(int);
    void setCurrencySymbol(const QString &);

protected:
    QString formatCurrency(double);

private:
    QSqlDatabase *m_pDatabase;
    QDate m_dateContractDate;
    QString m_sPathTemplate;
    QString m_sOutputPathDirectory;
    QString m_sCurrencySymbol;
    int m_iCurrencyDecimalPlaces;
    Person *m_pPerson;
    ContractNumerator *m_pContractNumerator;
    QList<MedicalServiceEntry *> m_pUslugi;
    QPrintPreviewDialog *m_pPrevDlg;
    QWebView *m_pWebView;
    QString generateContractNumber();
    int m_iCountMedicalServices;
    int save();
};
