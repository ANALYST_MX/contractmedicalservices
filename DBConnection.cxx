#include "DBConnection.hxx"

DBConnection::DBConnection()
    : m_iPort(-1)
{
    Configure *pConf = Singleton<Configure>::instance();
    setDatabaseName(pConf->getDatabaseNameDB());
    setDriverName(pConf->getDriverNameDB());
    setHostName(pConf->getHostNameDB());
    setPassword(pConf->getPasswordDB());
    setPort(pConf->getPortDB());
    setUserName(pConf->getUserNameDB());
}

DBConnection::~DBConnection()
{
    close();
}

void DBConnection::close()
{
    if (isOpen())
    {
        db.close();
    }
}

void DBConnection::connect() throw(SqlDatabaseException)
{
    if (QSqlDatabase::connectionNames().isEmpty())
    {
        db = QSqlDatabase::addDatabase(getDriverName());
    }
    db.setDatabaseName(getDatabaseName());
    db.setUserName(getUserName());
    db.setPassword(getPassword());
    db.setHostName(getHostName());
    db.setPort(getPort());
    if (!db.isValid())
    {
        throw SqlDatabaseException("The database subscription configuration is not valid");
    }
    if (!db.open())
    {
        throw SqlDatabaseException("Database cannot be open");
    }

}

void DBConnection::reconnect() throw(SqlDatabaseException)
{
    close();
    QSqlDatabase::removeDatabase(QSqlDatabase::database().connectionName());
    Configure *pConf = Singleton<Configure>::instance();
    setDriverName(pConf->getDriverNameDB());
    setDatabaseName(pConf->getDatabaseNameDB());
    setDriverName(pConf->getDriverNameDB());
    setHostName(pConf->getHostNameDB());
    setPassword(pConf->getPasswordDB());
    setPort(pConf->getPortDB());
    setUserName(pConf->getUserNameDB());
    connect();
}

bool DBConnection::isOpen() const
{
    return db.isOpen();
}

QString DBConnection::getDriverName() const
{
    return m_sDriverName;
}

QString DBConnection::getDatabaseName() const
{
    return m_sDatabaseName;
}

QString DBConnection::getUserName() const
{
    return m_sUserName;
}

QString DBConnection::getPassword() const
{
    return m_sPassword;
}

QString DBConnection::getHostName() const
{
    return m_sHostName;
}

int DBConnection::getPort() const
{
    return m_iPort;
}

void DBConnection::setDriverName(const QString & driver)
{
    m_sDriverName = driver;
}

void DBConnection::setDatabaseName(const QString & db_name)
{
    m_sDatabaseName = db_name;
}

void DBConnection::setUserName(const QString & user_name)
{
    m_sUserName = user_name;
}

void DBConnection::setPassword(const QString & password)
{
    m_sPassword = password;
}

void DBConnection::setHostName(const QString & host_name)
{
    m_sHostName = host_name;
}

void DBConnection::setPort(int i)
{
    m_iPort = i;
}

bool DBConnection::isValid() const
{
    return (!m_sDriverName.isEmpty() && !m_sDatabaseName.isEmpty());
}

DBConnection *DBConnection::instance()
{
    return Singleton<DBConnection>::instance();
}

QSqlDatabase *DBConnection::getDatabase()
{
    return &db;
}
