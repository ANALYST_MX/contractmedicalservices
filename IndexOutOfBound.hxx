#pragma once

#include <exception>
#include <sstream>

class IndexOutOfBound : public std::exception
{
public:
    IndexOutOfBound(const int);
    ~IndexOutOfBound() throw();
    const char *what() const throw();
private:
    int m_iIndex;
};
