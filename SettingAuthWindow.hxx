#pragma once

#include <QDialog>
#include <QtWidgets>

#include "Singleton.hxx"
#include "Configure.hxx"
#include "UrlUtil.hxx"

class SettingAuthWindow : public QDialog
{
    Q_OBJECT
public:
    explicit SettingAuthWindow(QWidget * = nullptr);
    void view();
    void closeEvent(QCloseEvent *);
    void showEvent(QShowEvent *);

public slots:
    void saveSettings();

private:
    Configure *m_pconf;
    QLineEdit *m_ptxtLogin;
    QLineEdit *m_ptxtPassword;
    QLineEdit *m_ptxtBaseURL;
    void readSettings();
};
