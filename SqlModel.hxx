#pragma once

#include <QString>
#include <QSqlQuery>
#include <QSqlError>
#include <QVariant>

#include "SqlException.hxx"
#include "SqlDatabaseException.hxx"
#include "LogicalFaultException.hxx"

class SqlModel
{
public:
    SqlModel(QSqlDatabase *, const QString &tableName, const QString &primaryKeyName = "id_pk");
    ~SqlModel();
    virtual void save() throw(SqlException) = 0;
    virtual void construct() throw(SqlException, LogicalFaultException) = 0;
    void construct(unsigned int) throw(SqlException, LogicalFaultException);
    void setPrimaryKey(unsigned int);
    const QString &table() const;
    const QString &keyfield() const;
    void setTable(const QString &);
    void setPrimaryKeyField(const QString &);
    unsigned int key() const;
    QSqlDatabase *db();
protected:
    void autoKey() throw(SqlException);
private:
    QSqlDatabase *m_pDatabase;
    QString m_sTable;
    QString m_sPrimaryKeyField;
    unsigned int m_iPrimaryKey;
};
