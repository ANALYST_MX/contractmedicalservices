#pragma once

#include <QAbstractListModel>

#include "MedicalServiceModel.hxx"
#include "DBQueries.hxx"
#include "IndexOutOfBound.hxx"

class MedicalServiceList : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit MedicalServiceList(QObject * = nullptr);
    QVariant data(const QModelIndex &, int = Qt::DisplayRole) const;
    int rowCount(const QModelIndex & = QModelIndex()) const;
    QHash<int, QByteArray> roleNames() const;
    MedicalServiceModel *get(int);

signals:
public slots:
    void findMedicalServices(const QString &);

private:
    QList<MedicalServiceModel *> m_medicalServices;
    enum MedicalServicesRoles {
        IdRole = Qt::UserRole + 1,
        CodeRole,
        NameRole,
        PriceRole
    };
    QHash<int, QByteArray> roles;
};
