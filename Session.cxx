#include "Session.hxx"

Session::Session(AuthPromed *auth, QObject *parent)
    : QObject(parent), m_pAuth(auth), m_iPeriod(5), m_bRunning(true)
{
    m_pConf = Singleton<Configure>::instance();
}

void Session::keepIn()
{
    while (m_bRunning)
    {
        qApp->processEvents();
        reloadPage();
        PortableSleep::sleepMinutes(m_iPeriod);
    }
}

void Session::reloadPage()
{
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    QUrl apiUrl = QString("%1").arg(m_pConf->getBaseURLAuth());
    apiUrl.setPath("/");
    QUrlQuery query;
    query.addQueryItem("c", "promed");
    apiUrl.setQuery(query);
    QNetworkRequest request(apiUrl);
    request.setRawHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; rv:31.0) Gecko/20100101 Firefox/31.0");
    request.setRawHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
    request.setRawHeader("Accept-Language", "en-US,en;q=0.5");
    request.setRawHeader("Accept-Encoding", "gzip, deflate");
    request.setRawHeader("Accept-Charset", "windows-1251,utf-8;q=0.7,*;q=0.7");
    request.setRawHeader("Keep-Alive", "300");
    request.setRawHeader("Connection", "keep-alive");
    request.setRawHeader("Cookie", m_pAuth->getAccessToken().toUtf8());
    QNetworkReply *reply = manager->get(request);
    connect(reply, SIGNAL(finished()), this, SLOT(reloadPageFinished()));
}

void Session::reloadPageFinished()
{
    QNetworkReply* reply = (QNetworkReply*)sender();
    if(reply->error() != QNetworkReply::NoError)
    {
        emit finished(true, false, reply->errorString());
    }
    else
    {
        if (reply->rawHeader("Content-Length").toInt() < 1024)
        {
            emit m_pAuth->logOn();
        }
    }
}

void Session::keepOut()
{
    if (m_bRunning)
    {
        m_bRunning = false;
        emit finished(false, true, "Aborted");
    }
}
