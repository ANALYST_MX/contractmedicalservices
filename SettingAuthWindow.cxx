#include "SettingAuthWindow.hxx"

SettingAuthWindow::SettingAuthWindow(QWidget *parent)
    : QDialog(parent)
{
    m_pconf = Singleton<Configure>::instance();
    view();
}

void SettingAuthWindow::view()
{
    QFormLayout *layout = new QFormLayout;
    layout->setMargin(5);
    layout->setSpacing(5);

    QLabel *lblLogin = new QLabel("Логин:");
    m_ptxtLogin = new QLineEdit;
    layout->addRow(lblLogin, m_ptxtLogin);

    QLabel *lblPassword = new QLabel("Пароль:");
    m_ptxtPassword = new QLineEdit;
    m_ptxtPassword->setEchoMode(QLineEdit::Password);
    layout->addRow(lblPassword, m_ptxtPassword);

    QLabel *lblBaseURL = new QLabel("URL:");
    m_ptxtBaseURL = new QLineEdit;
    layout->addRow(lblBaseURL, m_ptxtBaseURL);

    QPushButton *pcmdCancel = new QPushButton("Cancel");
    QPushButton *pcmdOk = new QPushButton("Ok");
    connect(pcmdCancel, SIGNAL(clicked()), SLOT(close()));
    connect(pcmdOk, SIGNAL(clicked()), SLOT(saveSettings()));
    layout->addRow(pcmdCancel, pcmdOk);

    setLayout(layout);
}

void SettingAuthWindow::closeEvent(QCloseEvent* evt)
{
    this->hide();
    evt->ignore();
}

void SettingAuthWindow::showEvent(QShowEvent *evt)
{
    readSettings();
    QWidget::showEvent(evt);
}

void SettingAuthWindow::saveSettings()
{
    m_pconf->setLoginAuth(m_ptxtLogin->text());
    m_pconf->setPasswordAuth(m_ptxtPassword->text());
    if (UrlUtil::parseURL(m_ptxtBaseURL->text().toStdString()).isValid())
    {
        m_pconf->setBaseURLAuth(m_ptxtBaseURL->text());
    }
    m_pconf->save();

    close();
}

void SettingAuthWindow::readSettings()
{
    m_ptxtLogin->setText(m_pconf->getLoginAuth());
    m_ptxtPassword->setText(m_pconf->getPasswordAuth());
    m_ptxtBaseURL->setText(m_pconf->getBaseURLAuth());
}
