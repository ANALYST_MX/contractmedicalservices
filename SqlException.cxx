#include "SqlException.hxx"

SqlException::SqlException(const char * what) throw()
    : m_sWhat(what)
{
}

SqlException::~SqlException() throw()
{
}

SqlException::SqlException(const SqlException & cpy)
{
    m_sWhat = cpy.m_sWhat;
}

const char *SqlException::what() const throw()
{
    return m_sWhat.c_str();
}
