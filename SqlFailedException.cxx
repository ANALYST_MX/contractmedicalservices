#include "SqlFailedException.hxx"

SqlFailedException::SqlFailedException(const char *what)
    : SqlException(what)
{
}
