#pragma once

#include <exception>
#include <string>

class SqlException
{
public:
    SqlException(const char *) throw();
    virtual ~SqlException() throw();
    SqlException(const SqlException &);
    virtual const char * what() const throw();
private:
    std::string m_sWhat;
};
