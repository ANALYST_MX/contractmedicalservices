#include "SqlDataNotFoundException.hxx"

SqlDataNotFoundException::SqlDataNotFoundException(const char *what)
    : SqlException(what)
{
}
