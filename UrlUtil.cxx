#include "UrlUtil.hxx"

UrlUtil::UrlUtil()
    : m_statusCode(Uninitialized)
{
}

UrlUtil::UrlUtil(UrlParserError errorCode)
    : m_statusCode(errorCode)
{
}

// check if the scheme name is valid
bool UrlUtil::isSchemeValid(const std::string &schemeName)
{
    for (auto c : schemeName)
    {
        if (!isalpha(c) && c != '+' && c != '-' && c != '.')
        {
            return false;
        }
    }

    return true;
}

std::string UrlUtil::encodeURL(const std::string &url)
{
    std::ostringstream escaped;
    escaped.fill('0');
    escaped << std::hex;

    for (std::string::const_iterator it = url.begin(), n = url.end(); it != n; ++it)
    {
        std::string::value_type c = *it;

        // Keep alphanumeric and other accepted characters intact
        if (isalnum(c) || c == '-' || c == '_' || c == '.' || c == '~')
        {
            escaped << c;
            continue;
        }

        // Any other characters are percent-encoded
        escaped << std::uppercase;
        escaped << '%' << std::setw(2) << int((unsigned char) c);
        escaped << std::nouppercase;
    }

    return escaped.str();
}

std::string UrlUtil::decodeURL(const std::string &encode)
{
    char buff[0x100] = {0};
    char* dst= buff;
    const char *data = encode.c_str();

    int value;
    int c;

    int len = encode.length();

    while (len--)
    {
        if (*data == '+')
        {
            *dst = ' ';
        }
        else if (*data == '%' && len >= 2
                 && isxdigit((int) *(data + 1))
                 && isxdigit((int) *(data + 2)))
        {
            c = ((unsigned char *)(data+1))[0];
            if (isupper(c))
            {
                c = tolower(c);
            }
            value = (c >= '0' && c <= '9' ? c - '0' : c - 'a' + 10) * 16;
            c = ((unsigned char *)(data+1))[1];
            if (isupper(c))
            {
                c = tolower(c);
            }
            value += c >= '0' && c <= '9' ? c - '0' : c - 'a' + 10;

            *dst = (char)value ;
            data += 2;
            len -= 2;
        }
        else
        {
            *dst = *data;
        }
        ++data;
        ++dst;
    }
    *dst = '\0';
    return buff;
}

// helper to convert the port number to int, return 'true' if the port is valid (within the 0..65535 range)
bool UrlUtil::parsePort(int *outPort) const
{
    if (!isValid())
    {
        return false;
    }

    int port = atoi(m_sPort.c_str());

    if (port <= 0 || port > 65535)
    {
        return false;
    }

    if (outPort)
    {
        *outPort = port;
    }

    return true;
}

std::string UrlUtil::getScheme() const
{
    return m_sScheme;
}

std::string UrlUtil::getHost() const
{
    return m_sHost;
}

std::string UrlUtil::getPort() const
{
    return m_sPort;
}

std::string UrlUtil::getPath() const
{
    return m_sPath;
}

std::string UrlUtil::getQuery() const
{
    return m_sQuery;
}

std::string UrlUtil::getFragment() const
{
    return m_sFragment;
}

std::string UrlUtil::getUserName() const
{
    return m_sUserName;
}

std::string UrlUtil::getPassword() const
{
    return m_sPassword;
}

UrlParserError UrlUtil::getErrorCode() const
{
    return m_statusCode;
}

// return 'true' if the parsing was successful
bool UrlUtil::isValid() const
{
    return m_statusCode == Ok;
}

// based on RFC 1738 and RFC 3986
UrlUtil UrlUtil::parseURL(const std::string &url)
{
    UrlUtil result;

    const char *currentString = url.c_str();

    /*
   *	<scheme>:<scheme-specific-part>
   *	<scheme> := [a-z\+\-\.]+
   *	For resiliency, programs interpreting URLs should treat upper case letters as equivalent to lower case in scheme names
   */

    // try to read scheme
    {
        const char *localString = strchr(currentString, ':');

        if (!localString)
        {
            return UrlUtil(NoUrlCharacter);
        }

        // save the scheme name
        result.m_sScheme = std::string(currentString, localString - currentString);

        if (!isSchemeValid(result.m_sScheme))
        {
            return UrlUtil(InvalidSchemeName);
        }

        // scheme should be lowercase
        std::transform(result.m_sScheme.begin(), result.m_sScheme.end(), result.m_sScheme.begin(), ::tolower);

        // skip ':'
        currentString = localString + 1;
    }

    /*
   *	//<user>:<password>@<host>:<port>/<url-path>
   *	any ":", "@" and "/" must be normalized
   */

    // skip "//"
    if (*currentString++ != '/')
    {
        return UrlUtil(NoDoubleSlash);
    }
    if (*currentString++ != '/')
    {
        return UrlUtil(NoDoubleSlash);
    }

    // check if the user name and password are specified
    bool bHasUserName = false;

    const char *localString = currentString;

    while (*localString)
    {
        if (*localString == '@')
        {
            // user name and password are specified
            bHasUserName = true;
            break;
        }
        else if (*localString == '/')
        {
            // end of <host>:<port> specification
            bHasUserName = false;
            break;
        }

        localString++;
    }

    // user name and password
    localString = currentString;

    if (bHasUserName)
    {
        // read user name
        while (*localString && *localString != ':' && *localString != '@')
        {
            ++localString;
        }

        result.m_sUserName = std::string(currentString, localString - currentString);

        // proceed with the current pointer
        currentString = localString;

        if (*currentString == ':')
        {
            // skip ':'
            ++currentString;

            // read password
            localString = currentString;

            while (*localString && *localString != '@')
            {
                localString++;
            }

            result.m_sPassword = std::string(currentString, localString - currentString);

            currentString = localString;
        }

        // skip '@'
        if (*currentString != '@')
        {
            return UrlUtil(NoAtSign);
        }

        ++currentString;
    }

    bool bHasBracket = (*currentString == '[');

    // go ahead, read the host name
    localString = currentString;

    while (*localString)
    {
        if (bHasBracket && *localString == ']')
        {
            // end of IPv6 address
            ++localString;
            break;
        }
        else if (!bHasBracket && (*localString == ':' || *localString == '/'))
        {
            // port number is specified
            break;
        }

        ++localString;
    }

    result.m_sHost = std::string(currentString, localString - currentString);

    currentString = localString;

    // is port number specified?
    if (*currentString == ':')
    {
        ++currentString;

        // read port number
        localString = currentString;

        while (*localString && *localString != '/')
        {
            ++localString;
        }

        result.m_sPort = std::string(currentString, localString - currentString);

        currentString = localString;
    }

    // end of string
    if (!*currentString)
    {
        return UrlUtil(UnexpectedEndOfLine);
    }

    // skip '/'
    if (*currentString != '/')
    {
        return UrlUtil(NoSlash);
    }

    ++currentString;

    // parse the path
    localString = currentString;

    while (*localString && *localString != '#' && *localString != '?')
    {
        ++localString;
    }

    result.m_sPath = std::string(currentString, localString - currentString);

    currentString = localString;

    // check for query
    if (*currentString == '?')
    {
        // skip '?'
        ++currentString;

        // read query
        localString = currentString;

        while (*localString && *localString != '#')
        {
            ++localString;
        }

        result.m_sQuery = std::string(currentString, localString - currentString);

        currentString = localString;
    }

    // check for fragment
    if (*currentString == '#')
    {
        // skip '#'
        ++currentString;

        // read fragment
        localString = currentString;

        while (*localString)
        {
            ++localString;
        }

        result.m_sFragment = std::string(currentString, localString - currentString);

        currentString = localString;
    }

    result.m_statusCode = Ok;

    return result;
}
