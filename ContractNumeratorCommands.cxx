#include "ContractNumeratorCommands.hxx"

#include <QSqlError>
#include <QVariant>

ContractNumeratorCommands::ContractNumeratorCommands(QSqlDatabase *db, QObject *parent)
    : QObject(parent), m_pDatabase(db)
{
}

bool ContractNumeratorCommands::insert(ContractNumerator *contractNumerator)
{
    if(m_pDatabase->isOpen())
    {
        QSqlQuery query;
        query.prepare(queryInsertNumerator);
        query.bindValue(":start_value", contractNumerator->getFromNumber());
        query.bindValue(":end_value", contractNumerator->getToNumber());
        query.bindValue(":from_date", contractNumerator->getStartDate().toString("dd.MM.yyyy"));
        query.bindValue(":serial", contractNumerator->getSerial());
        query.bindValue(":number_length", contractNumerator->getNumberLength());
        bool succeeded = query.exec();
        if(!succeeded)
        {
            m_sLastError = query.lastError().text();
            return succeeded;
        }

        succeeded = updateStartIntervalValue(contractNumerator->getFromNumber());

        return succeeded;
    }
    else
    {
        throw SqlDatabaseException("Database is not open");
    }

    return false;
}

bool ContractNumeratorCommands::update(ContractNumerator *contractNumerator)
{
    if(m_pDatabase->isOpen())
    {
        QSqlQuery query;
        query.prepare(queryUpdateNumerator);
        query.bindValue(":start_value", contractNumerator->getFromNumber());
        query.bindValue(":end_value", contractNumerator->getToNumber());
        query.bindValue(":from_date", contractNumerator->getStartDate().toString("dd.MM.yyyy"));
        query.bindValue(":serial", contractNumerator->getSerial());
        query.bindValue(":number_length", contractNumerator->getNumberLength());
        bool succeeded = query.exec();
        if(!succeeded)
        {
            m_sLastError = query.lastError().text();
            return succeeded;
        }

        int currentValue = incrementNumber();
        if (contractNumerator->getFromNumber() < currentValue || contractNumerator->getFromNumber() > currentValue)
        {
            succeeded = updateStartIntervalValue(contractNumerator->getFromNumber());
        }

        return succeeded;
    }
    else
    {
        throw SqlDatabaseException("Database is not open");
    }

    return false;
}

ContractNumerator *ContractNumeratorCommands::select(QDate startDate)
{
    ContractNumerator *contractNumerator = nullptr;
    if(m_pDatabase->isOpen())
    {
        QSqlQuery query;
        bool succeeded = query.exec(querySelectNumeratorInDate.arg(startDate.toString("dd.MM.yyyy")));
        if(!succeeded)
        {
            m_sLastError = query.lastError().text();
        }
        if (query.next())
        {
            contractNumerator = new ContractNumerator;
            contractNumerator->setStartDate(query.value("start_date").toDate());
            contractNumerator->setFromNumber(query.value("start_interval_range").toInt());
            contractNumerator->setToNumber(query.value("end_interval_range").toInt());
            contractNumerator->setSerial(query.value("contract_serial").toString());
            contractNumerator->setNumberLength(query.value("contract_number_length").toInt());
        }
    }
    else
    {
        throw SqlDatabaseException("Database is not open");
    }

    return contractNumerator;
}

ContractNumerator *ContractNumeratorCommands::find(QDate startDate)
{
    ContractNumerator *contractNumerator = nullptr;
    if(m_pDatabase->isOpen())
    {
        QSqlQuery query;
        bool succeeded = query.exec(querySelectNumeratorByDate.arg(startDate.toString("dd.MM.yyyy")));
        if(!succeeded)
        {
            m_sLastError = query.lastError().text();
        }
        if (query.next())
        {
            contractNumerator = new ContractNumerator;
            contractNumerator->setStartDate(query.value("start_date").toDate());
            contractNumerator->setFromNumber(query.value("start_interval_range").toInt());
            contractNumerator->setToNumber(query.value("end_interval_range").toInt());
            contractNumerator->setSerial(query.value("contract_serial").toString());
            contractNumerator->setNumberLength(query.value("contract_number_length").toInt());
        }
    }
    else
    {
        throw SqlDatabaseException("Database is not open");
    }

    return contractNumerator;
}

bool ContractNumeratorCommands::updateStartIntervalValue(int newValue)
{
    if(m_pDatabase->isOpen())
    {
        QSqlQuery query;
        bool succeeded = query.exec(queryInsertContractStartIntervalValue.arg(newValue));
        if(!succeeded)
        {
            m_sLastError = query.lastError().text();
            return false;
        }

        return succeeded;
    }
    else
    {
        throw SqlDatabaseException("Database is not open");
    }

    return false;
}

int ContractNumeratorCommands::incrementNumber()
{
    int res = -1;
    if (m_pDatabase->isOpen())
    {
        QSqlQuery query;
        query.exec(querySelectContractCurrentIntervalValue);
        if (query.next())
        {
            res = query.value("contract_number").toInt();
        }
        query.finish();
    }
    else
    {
        throw SqlDatabaseException("Database is not open");
    }

    return res;
}

QString ContractNumeratorCommands::getLastError()
{
    return m_sLastError;
}
