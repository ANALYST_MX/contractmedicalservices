### Формирование договора платных медицинских услуг. Услуги берутся из БД. Договор формируется из шаблона Word - документа. Персональные данные клиента загружаются с РМИАС Промед. ###

**Настройка доступа к РМИАС Промед:**

* Логин
* Пароль
* Ссылка на сайт РМИАС Промед

**Настройка подключения к БД:**

* Драйвер - QPSQL
* Имя базы данных - rkbkuv_database
* Пользователь
* Пароль
* IP
* Port

**Настройка печати:**

* Путь к шаблону договора
* Временная папка для сохранения договоров

**Настройка договора:**

* Серия
* Длина номера
* Начало интервала номеров
* Конец интервала номеров
* Дата с которого номера актуальны

**Шаблоны для замены:**

* {{contract_date}} - Дата договора
* {{contract_number}} - Номер договора
* {{surname}} - Фамилия пациента
* {{name}} - Имя пациента
* {{patronymic}} - Отчество пациента
* {{address}} - Адрес пациента
* {{phone}} - Телефон пациента
* {{birthday}} - День рождения пациента
* {{document_number}} - Номер документа пациента
* {{document_serial}} - Серия документа пациента
* {{document_type}} - Тип документа пациента
* {{document_date_of_issue}} - Дата выдачи документа пациента
* {{document_authority}} - Кто выдал документ пациента
* {{representative_surname}} - Фамилия представителя
* {{representative_name}} - Имя представителя
* {{representative_patronymic}} - Отчество представителя
* {{representative_address}} - Адрес представителя
* {{representative_phone}} - Телефон представителя
* {{representative_birthday}} - День рождения представителя
* {{representative_document_number}} - Номер документа представителя
* {{representative_document_serial}} - Серия документа представителя
* {{representative_document_type}} - Тип документа представителя
* {{representative_document_date_of_issue}} - Дата выдачи документа представителя
* {{representative_document_authority}} - Кто выдал документ представителя
* {{surname|representative_surname}} - Фамилия пациента или представителя
* {{name|representative_name}} - Имя пациента или представителя
* {{patronymic|representative_patronymic}} - Отчество пациента или представителя
* {{address|representative_address}} - Адрес пациента или представителя
* {{phone|representative_phone}} - Телефон пациента или представителя
* {{birthday|representative_birthday}} - День рождения пациента или представителя
* {{document_number|representative_document_number}} - Номер документа пациента или представителя
* {{document_serial|representative_document_serial}} - Серия документа пациента или представителя
* {{document_type|representative_document_type}} - Тип документа пациента или представителя
* {{document_date_of_issue|representative_document_date_of_issue}} - Дата выдачи документа пациента или представителя
* {{document_authority|representative_document_authority}} - Кто выдал документ пациента или представителя
* {{usluga_code_N}} - Код услуги "N", где N-порядновый номер
* {{usluga_name_N}} - Наименование услуги "N", где N-порядновый номер
* {{usluga_count_N}} - Количество услуги "N", где N-порядновый номер
* {{usluga_date_expires_N}} - Дата выполнения услуги "N", где N-порядновый номер
* {{usluga_price_N}} - Цена услуги "N", где N-порядновый номер
* {{usluga_cost_N}} - Стоимость услуги с учетом количества "N", где N-порядновый номер
* {{total_cost}} - Конечная сумма услуг