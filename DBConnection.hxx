#pragma once

#include <QString>
#include <QSqlDatabase>

#include "Singleton.hxx"
#include "SqlDatabaseException.hxx"
#include "Configure.hxx"

class DBConnection
{
    friend class Singleton<DBConnection>;
public:
    ~DBConnection();
    QString getDriverName() const;
    QString getDatabaseName() const;
    QString getUserName() const;
    QString getPassword() const;
    QString getHostName() const;
    int getPort() const;
    void setDriverName(const QString &);
    void setDatabaseName(const QString &);
    void setUserName(const QString &);
    void setPassword(const QString &);
    void setHostName(const QString &);
    void setPort(int);
    bool isValid() const;
    void connect() throw(SqlDatabaseException);
    void reconnect() throw(SqlDatabaseException);
    void close();
    bool isOpen() const;
    QSqlDatabase *getDatabase();

public:
    static DBConnection *instance();

private:
    DBConnection();
    QString m_sDriverName;
    QString m_sDatabaseName;
    QString m_sUserName;
    QString m_sPassword;
    QString m_sHostName;
    int m_iPort;
    QSqlDatabase db;
};
