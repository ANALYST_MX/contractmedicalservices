#include "SettingPrintWindow.hxx"
#include "FilesystemAdapter.hxx"

SettingPrintWindow::SettingPrintWindow(QWidget *parent)
    : QDialog(parent)
{
    m_pconf = Singleton<Configure>::instance();
    view();
}

void SettingPrintWindow::view()
{
    QFormLayout *layout = new QFormLayout;
    layout->setMargin(5);
    layout->setSpacing(5);

    QLabel *lblEmpateFilePath = new QLabel("Путь:");
    m_ptxtEmpateFilePath = new QLineEdit;
    layout->addRow(lblEmpateFilePath, m_ptxtEmpateFilePath);
    QLabel *lblTempPath = new QLabel("Временная папка:");
    m_ptxtTempPath = new QLineEdit;
    layout->addRow(lblTempPath, m_ptxtTempPath);
    QLabel *lblDecimalPlaces = new QLabel("Число десятичных знаков:");
    m_pspbDecimalPlaces = new QSpinBox;
    m_pspbDecimalPlaces->setMinimum(0);
    m_pspbDecimalPlaces->setMaximum(10);
    layout->addRow(lblDecimalPlaces, m_pspbDecimalPlaces);
    QLabel *lblSymbol = new QLabel("Обозначение:");
    m_ptxtSymbol = new QLineEdit;
    layout->addRow(lblSymbol, m_ptxtSymbol);
    QLabel *lblCountMedicalServices = new QLabel("Число услуг:");
    m_pspbCountMedicalServices = new QSpinBox;
    m_pspbCountMedicalServices->setMinimum(0);
    m_pspbCountMedicalServices->setMaximum(99);
    layout->addRow(lblCountMedicalServices, m_pspbCountMedicalServices);

    QPushButton *pcmdCancel = new QPushButton("Cancel");
    QPushButton *pcmdOk = new QPushButton("Ok");
    connect(pcmdCancel, SIGNAL(clicked()), SLOT(close()));
    connect(pcmdOk, SIGNAL(clicked()), SLOT(saveSettings()));
    layout->addRow(pcmdCancel, pcmdOk);

    setLayout(layout);
}

void SettingPrintWindow::closeEvent(QCloseEvent* evt)
{
    this->hide();
    evt->ignore();
}

void SettingPrintWindow::showEvent(QShowEvent *evt)
{
    readSettings();
    QWidget::showEvent(evt);
}

void SettingPrintWindow::saveSettings()
{
    if (!FilesystemAdapter::fileExists(m_ptxtEmpateFilePath->text()))
    {
        QMessageBox msgBox;
        msgBox.setText(QString("Путь <b>%1</b> не найден!").arg(m_ptxtEmpateFilePath->text()));
        msgBox.exec();
        m_ptxtEmpateFilePath->setFocus();
    }
    else if (!FilesystemAdapter::fileExists(m_ptxtTempPath->text()))
    {
        QMessageBox msgBox;
        msgBox.setText(QString("Временная папка <b>%1</b> не найден!").arg(m_ptxtTempPath->text()));
        msgBox.exec();
        m_ptxtEmpateFilePath->setFocus();
    }
    else
    {
        m_pconf->setPathTempateFilePrint(m_ptxtEmpateFilePath->text());
        m_pconf->setPathTempDirectory(m_ptxtTempPath->text());
        m_pconf->setCurrencyDecimalPlaces(m_pspbDecimalPlaces->value());
        m_pconf->setCurrencySymbol(m_ptxtSymbol->text());
        m_pconf->setCountMedicalServices(m_pspbCountMedicalServices->value());
        m_pconf->save();

        close();
    }
}

void SettingPrintWindow::readSettings()
{
    m_ptxtEmpateFilePath->setText(m_pconf->getPathTempateFilePrint());
    m_ptxtTempPath->setText(m_pconf->getPathTempDirectory());
    m_pspbDecimalPlaces->setValue(m_pconf->getCurrencyDecimalPlaces());
    m_ptxtSymbol->setText(m_pconf->getCurrencySymbol());
    m_pspbCountMedicalServices->setValue(m_pconf->getCountMedicalServices());
}
