#include "Configure.hxx"

Configure::Configure(QObject *parent)
    : QObject(parent)
{
    load();
}

void Configure::save()
{
    m_settings->beginGroup("/Settings/Auth");
    m_settings->setValue("/login", m_sLoginAuth);
    m_settings->setValue("/password", m_sPasswordAuth);
    m_settings->setValue("/baseURL", m_sBaseURLAuth);
    m_settings->endGroup();
    m_settings->beginGroup("/Settings/DB");
    m_settings->setValue("/databaseName", m_sDatabaseNameDB);
    m_settings->setValue("/driverName", m_sDriverNameDB);
    m_settings->setValue("/hostName", m_sHostNameDB);
    m_settings->setValue("/password", m_sPasswordDB);
    m_settings->setValue("/userName", m_sUserNameDB);
    m_settings->setValue("/port", m_iPortDB);
    m_settings->endGroup();
    m_settings->beginGroup("/Settings/Print");
    m_settings->setValue("/empateFilePrint", m_sPathTempateFilePrint);
    m_settings->setValue("/tempDirectory", m_sPathTempDirectory);
    m_settings->setValue("/currencyDecimalPlaces", m_iCurrencyDecimalPlaces);
    m_settings->setValue("/countMedicalServices", m_iCountMedicalServices);
    m_settings->setValue("/currencySymbol", m_sCurrencySymbol);
    m_settings->endGroup();
}

void Configure::load()
{
    m_settings = App::theApp()->getSettings();
    m_settings->beginGroup("/Settings/Auth");
    m_sLoginAuth = m_settings->value("/login", "").toString();
    m_sPasswordAuth = m_settings->value("/password", "").toString();
    m_sBaseURLAuth = m_settings->value("/baseURL", "").toString();
    m_settings->endGroup();
    m_settings->beginGroup("/Settings/DB");
    m_sDatabaseNameDB = m_settings->value("/databaseName", "").toString();
    m_sDriverNameDB = m_settings->value("/driverName", "").toString();
    m_sHostNameDB = m_settings->value("/hostName", "").toString();
    m_sPasswordDB = m_settings->value("/password", "").toString();
    m_sUserNameDB = m_settings->value("/userName", "").toString();
    m_iPortDB = m_settings->value("/port", 0).toInt();
    m_settings->endGroup();
    m_settings->beginGroup("/Settings/Print");
    m_sPathTempateFilePrint = m_settings->value("/empateFilePrint", "").toString();
    m_sPathTempDirectory = m_settings->value("/tempDirectory", "").toString();
    m_iCurrencyDecimalPlaces = m_settings->value("/currencyDecimalPlaces", 2).toInt();
    m_iCountMedicalServices = m_settings->value("/countMedicalServices", 0).toInt();
    m_sCurrencySymbol = m_settings->value("/currencySymbol", "").toString();
    m_settings->endGroup();
}

void Configure::setLoginAuth(const QString &value)
{
    m_sLoginAuth = value;
}

QString Configure::getLoginAuth()
{
    return m_sLoginAuth;
}

void Configure::setPasswordAuth(const QString &value)
{
    m_sPasswordAuth = value;
}

QString Configure::getPasswordAuth()
{
    return m_sPasswordAuth;
}

void Configure::setBaseURLAuth(const QString &value)
{
    m_sBaseURLAuth = value;
}

QString Configure::getBaseURLAuth()
{
    return m_sBaseURLAuth;
}

QString Configure::getDriverNameDB() const
{
    return m_sDriverNameDB;
}

QString Configure::getDatabaseNameDB() const
{
    return m_sDatabaseNameDB;
}

QString Configure::getUserNameDB() const
{
    return m_sUserNameDB;
}

QString Configure::getPasswordDB() const
{
    return m_sPasswordDB;
}

QString Configure::getHostNameDB() const
{
    return m_sHostNameDB;
}

int Configure::getPortDB() const
{
    return m_iPortDB;
}

QString Configure::getPathTempateFilePrint() const
{
    return m_sPathTempateFilePrint;
}

QString Configure::getPathTempDirectory() const
{
    return m_sPathTempDirectory;
}

int Configure::getCurrencyDecimalPlaces() const
{
    return m_iCurrencyDecimalPlaces;
}

QString Configure::getCurrencySymbol() const
{
    return m_sCurrencySymbol;
}

int Configure::getCountMedicalServices() const
{
    return m_iCountMedicalServices;
}

void Configure::setDriverNameDB(const QString &driver)
{
    m_sDriverNameDB = driver;
}

void Configure::setDatabaseNameDB(const QString &dbName)
{
    m_sDatabaseNameDB = dbName;
}

void Configure::setUserNameDB(const QString &userName)
{
    m_sUserNameDB = userName;
}

void Configure::setPasswordDB(const QString &password)
{
    m_sPasswordDB = password;
}

void Configure::setHostNameDB(const QString &hostName)
{
    m_sHostNameDB = hostName;
}

void Configure::setPortDB(int i)
{
    m_iPortDB = i;
}

void Configure::setPathTempateFilePrint(const QString &path)
{
    m_sPathTempateFilePrint = path;
}

void Configure::setPathTempDirectory(const QString &path)
{
    m_sPathTempDirectory = path;
}

void Configure::setCurrencyDecimalPlaces(int decimalPlaces)
{
    m_iCurrencyDecimalPlaces = decimalPlaces;
}

void Configure::setCurrencySymbol(const QString &symbol)
{
    m_sCurrencySymbol = symbol;
}

void Configure::setCountMedicalServices(int countMedicalServices)
{
    m_iCountMedicalServices = countMedicalServices;
}
