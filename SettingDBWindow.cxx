#include "SettingDBWindow.hxx"

SettingDBWindow::SettingDBWindow(QWidget *parent)
    : QDialog(parent)
{
    m_pconf = Singleton<Configure>::instance();
    view();
}

void SettingDBWindow::view()
{
    QFormLayout *layout = new QFormLayout;
    layout->setMargin(5);
    layout->setSpacing(5);

    QLabel *lblDriverName = new QLabel("Драйвер:");
    QStringList drivers = QSqlDatabase::drivers();
    drivers.removeAll("QMYSQL3");
    drivers.removeAll("QOCI8");
    drivers.removeAll("QODBC3");
    drivers.removeAll("QPSQL7");
    drivers.removeAll("QTDS7");
    m_pcboDriverName = new QComboBox;
    m_pcboDriverName->addItems(drivers);
    if (drivers.contains("QPSQL"))
    {
        auto index = m_pcboDriverName->findText("QPSQL");
        m_pcboDriverName->setCurrentIndex(index);
    }
    layout->addRow(lblDriverName, m_pcboDriverName);

    QLabel *lblDatabaseName = new QLabel("Имя базы данных:");
    m_ptxtDatabaseName = new QLineEdit;
    layout->addRow(lblDatabaseName, m_ptxtDatabaseName);

    QLabel *lblUserName = new QLabel("Пользователь:");
    m_ptxtUserName = new QLineEdit;
    layout->addRow(lblUserName, m_ptxtUserName);

    QLabel *lblPassword = new QLabel("Пароль:");
    m_ptxtPassword = new QLineEdit;
    m_ptxtPassword->setEchoMode(QLineEdit::Password);
    layout->addRow(lblPassword, m_ptxtPassword);

    QLabel *lblHostName = new QLabel("Адрес:");
    m_ptxtHostName = new QLineEdit;
    layout->addRow(lblHostName, m_ptxtHostName);

    QLabel *lblPort = new QLabel("Порт:");
    m_pspbPort = new QSpinBox;
    m_pspbPort->setMinimum(0);
    m_pspbPort->setMaximum(65535);
    layout->addRow(lblPort, m_pspbPort);

    QPushButton *pcmdCancel = new QPushButton("Cancel");
    QPushButton *pcmdOk = new QPushButton("Ok");
    connect(pcmdCancel, SIGNAL(clicked()), SLOT(close()));
    connect(pcmdOk, SIGNAL(clicked()), SLOT(saveSettings()));
    layout->addRow(pcmdCancel, pcmdOk);

    setLayout(layout);
}

void SettingDBWindow::closeEvent(QCloseEvent* evt)
{
    this->hide();
    evt->ignore();
}

void SettingDBWindow::showEvent(QShowEvent *evt)
{
    readSettings();
    QWidget::showEvent(evt);
}

void SettingDBWindow::saveSettings()
{
    m_pconf->setDriverNameDB(m_pcboDriverName->currentText());
    m_pconf->setDatabaseNameDB(m_ptxtDatabaseName->text());
    m_pconf->setUserNameDB(m_ptxtUserName->text());
    m_pconf->setPasswordDB(m_ptxtPassword->text());
    m_pconf->setHostNameDB(m_ptxtHostName->text());
    m_pconf->setPortDB(m_pspbPort->value());
    m_pconf->save();

    try
    {
        DBConnection::instance()->reconnect();
    }
    catch (SqlDatabaseException &)
    {
    }

    close();
}

void SettingDBWindow::readSettings()
{
    int index = m_pcboDriverName->findText(m_pconf->getDriverNameDB());
    if ( index != -1 )
    {
        m_pcboDriverName->setCurrentIndex(index);
    }
    m_ptxtDatabaseName->setText(m_pconf->getDatabaseNameDB());
    m_ptxtUserName->setText(m_pconf->getUserNameDB());
    m_ptxtPassword->setText(m_pconf->getPasswordDB());
    m_ptxtHostName->setText(m_pconf->getHostNameDB());
    m_pspbPort->setValue(m_pconf->getPortDB());
}
