#pragma once

#include <QString>

/**
 *  ** medical_services **
 *
 *  CREATE TABLE public.medical_services
 *  (
 *    id_pk BIGSERIAL PRIMARY KEY NOT NULL,
 *    code_medical_service character(16),
 *    name_medical_service text,
 *    price_medical_service money
 *  );
 *
 *  CREATE UNIQUE INDEX medical_services_id_pk_unique_index ON public.medical_services (id_pk);
 */

/**
 *  ** numerators **
 *
 *  CREATE TABLE public.numerators
 *  (
 *    id_pk BIGSERIAL PRIMARY KEY NOT NULL,
 *    start_interval_range integer,
 *    end_interval_range integer,
 *    start_date date,
 *    contract_serial text,
 *    contract_number_length integer
 *  );
 *
 *  CREATE UNIQUE INDEX numerators_id_pk_unique_index ON public.numerators (id_pk);
 */

/**
 *  ** contracts **
 *
 *  CREATE SEQUENCE contract_number_seq;
 *  CREATE TABLE public.contracts
 *  (
 *    id_pk BIGSERIAL PRIMARY KEY NOT NULL,
 *    contract_number integer,
 *    contract_date date
 *  );
 *
 *  CREATE UNIQUE INDEX contracts_id_pk_unique_index ON public.contracts (id_pk);
 *  ALTER SEQUENCE contract_number_seq OWNED BY contracts.contract_number;
 */

const QString querySelectMedicalServiceById =\
        "SELECT id_pk, code_medical_service, name_medical_service, price_medical_service "
        "FROM medical_services "
        "WHERE id_pk = %1;";

const QString querySelectMedicalServicesByCodeOrName =\
        "SELECT id_pk, code_medical_service, name_medical_service, price_medical_service::money::numeric::real "
        "FROM medical_services "
        "WHERE code_medical_service ILIKE '\%%1\%' "
        "OR "
        "name_medical_service ILIKE '\%%1\%' "
        "ORDER BY name_medical_service ASC;";

const QString queryInsertMedicalService =\
        "INSERT INTO medical_services "
        "(code_medical_service, name_medical_service, price_medical_service) "
        "VALUES(:code, :name, :price);";

const QString queryUpdateMedicalService =\
        "UPDATE medical_services SET "
        "code_medical_service=:code, name_medical_service=:name, price_medical_service=:price "
        "WHERE id_pk = :id;";

const QString queryDeleteMedicalServiceById =\
        "DELETE FROM medical_services WHERE id_pk = %1";

const QString queryInsertNumerator =\
        "INSERT INTO numerators "
        "(start_interval_range, end_interval_range, start_date, contract_serial, contract_number_length) "
        "VALUES(:start_value, :end_value, :from_date, :serial, :number_length);";

const QString querySelectNumeratorInDate =\
        "SELECT start_interval_range, end_interval_range, start_date, contract_serial, contract_number_length "
        "FROM numerators "
        "WHERE start_date <= '%1' "
        "ORDER BY start_date DESC "
        "LIMIT 1;";

const QString querySelectNumeratorByDate =\
        "SELECT start_interval_range, end_interval_range, start_date, contract_serial, contract_number_length "
        "FROM numerators "
        "WHERE start_date = '%1' "
        "ORDER BY start_date DESC "
        "LIMIT 1;";

const QString queryDeleteNumeratorById =\
        "DELETE FROM history_interval WHERE id_pk = %1";

const QString queryUpdateNumerator =\
        "UPDATE numerators SET "
        "start_interval_range=:start_value, end_interval_range=:end_value, contract_serial=:serial, contract_number_length=:number_length "
        "WHERE start_date = :from_date;";

const QString queryInsertContractStartIntervalValue =\
        "SELECT setval('contract_number_seq', %1);";

const QString queryInsertContract =\
        "INSERT INTO contracts "
        "(contract_date) "
        "VALUES(:created_on) "
        "RETURNING contract_number;";

const QString querySelectContractCurrentIntervalValue =\
        "SELECT NEXTVAL('contract_number_seq') AS contract_number;";
