#pragma once

#include <QObject>
#include <QHash>

#include "MedicalServiceEntry.hxx"

class CartMedicalServiceList : public QObject
{
    Q_OBJECT
public:
    explicit CartMedicalServiceList(QObject * = nullptr);
    void clear();
    bool contains(const QString &) const;
    QHash<QString, MedicalServiceEntry *>::iterator begin();
    QHash<QString, MedicalServiceEntry *>::iterator end();
    void set(const QString &, MedicalServiceEntry *);
    MedicalServiceEntry *get(const QString &);
    void remove(const QString &);
    int size() const;

private:
    QHash<QString, MedicalServiceEntry *> m_medicalServices;
};
