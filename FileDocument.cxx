#include "FileDocument.hxx"

FileDocument::FileDocument(const QString &path, QObject *parent)
    : QObject(parent), m_sPath(path)
{
}

FileDocument::~FileDocument()
{
}

QString FileDocument::copyToDir(const QString &newPath)
{
    QTemporaryFile *tempFile = new QTemporaryFile(QDir(newPath).absoluteFilePath(QFileInfo(m_sPath).baseName() + "_XXXXXXX." + QFileInfo(m_sPath).suffix()));
    tempFile->setAutoRemove(true);
    tempFile->open();
    QString tempFilePath = tempFile->fileName();
    tempFile->close();
    delete tempFile;
    QFile::copy(m_sPath, tempFilePath);

    return tempFilePath;
}

QString FileDocument::getPath() const
{
    return m_sPath;
}

void FileDocument::setPath(const QString &newPath)
{
    m_sPath = newPath;
}
