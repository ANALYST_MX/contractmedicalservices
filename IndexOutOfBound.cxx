#include "IndexOutOfBound.hxx"

IndexOutOfBound::IndexOutOfBound(const int index)
    : m_iIndex(index)
{
}

IndexOutOfBound::~IndexOutOfBound()throw() {
}

const char *IndexOutOfBound::what() const throw() {
    std::ostringstream msg;
    msg << "IndexOutOfBound: " << m_iIndex;
    return msg.str().c_str();
}
