#pragma once

#include "SqlException.hxx"

class SqlDatabaseException : public SqlException
{
public:
    SqlDatabaseException(const char *);
};
