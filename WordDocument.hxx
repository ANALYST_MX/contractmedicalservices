#pragma once

#include <QObject>
#include <QAxObject>
#include <QAxWidget>
#include <memory>
#include <thread>

#include "FileDocument.hxx"

class WordDocument : public FileDocument
{
    Q_OBJECT
public:
    explicit WordDocument(const QString &, QObject * = nullptr);
    ~WordDocument();
    QList<QVariant> retrieveDefaultParam(bool = true);
    bool isOpen() const;
    void substitution(const QString &, const QString &, bool = true);
    void saveAs(const QString &);
    void save();
    void close();
    void setVisible(bool);
    void print() const;
    void open();

signals:

public slots:

private:
    QAxObject *m_pWordApplication;
    QAxObject *m_pWordDocuments;
    QAxObject *m_pWordDocument;
};
