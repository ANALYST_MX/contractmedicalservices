#pragma once

#include <QDialog>
#include <QtWidgets>

#include "Singleton.hxx"
#include "Configure.hxx"

class SettingPrintWindow : public QDialog
{
    Q_OBJECT
public:
    explicit SettingPrintWindow(QWidget * = 0);
    void view();
    void closeEvent(QCloseEvent *);
    void showEvent(QShowEvent *);

signals:

public slots:
    void saveSettings();

private:
    void readSettings();
    Configure *m_pconf;
    QLineEdit *m_ptxtEmpateFilePath;
    QLineEdit *m_ptxtTempPath;
    QSpinBox *m_pspbDecimalPlaces;
    QLineEdit *m_ptxtSymbol;
    QSpinBox *m_pspbCountMedicalServices;
};
