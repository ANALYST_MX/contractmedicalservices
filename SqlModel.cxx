#include "SqlModel.hxx"

SqlModel::SqlModel(QSqlDatabase* db, const QString &tablename, const QString &primarykeyname)
    : m_pDatabase(db),
      m_sTable(tablename),
      m_sPrimaryKeyField(primarykeyname),
      m_iPrimaryKey(0)
{
}

SqlModel::~SqlModel()
{
}

void SqlModel::construct(unsigned int fromid) throw(SqlException, LogicalFaultException)
{
    if(!fromid)
    {
        throw LogicalFaultException("Cannot construct a SqlModel from an id = 0");
    }
    setPrimaryKey(fromid);
    construct();
}

void SqlModel::autoKey() throw(SqlException)
{
    if(db()->isOpen())
    {
        QSqlQuery query(QString("SELECT MAX(%1)+1 FROM %2").arg(m_sPrimaryKeyField).arg(m_sTable));
        if(query.next())
        {
            m_iPrimaryKey = query.value(0).toUInt();
        }
        else
        {
            m_iPrimaryKey = 1;
        }
    }
    else
    {
        throw SqlDatabaseException("Database is not open");
    }
}

void SqlModel::setTable(const QString &table)
{
    m_sTable = table;
}

void SqlModel::setPrimaryKeyField(const QString &keyName)
{
    m_sPrimaryKeyField = keyName;
}

void SqlModel::setPrimaryKey(unsigned int pk)
{
    m_iPrimaryKey = pk;
}

const QString& SqlModel::table() const
{
    return m_sTable;
}

const QString& SqlModel::keyfield() const
{
    return m_sPrimaryKeyField;
}

unsigned int SqlModel::key() const
{
    return m_iPrimaryKey;
}

QSqlDatabase *SqlModel::db()
{
    return m_pDatabase;
}
