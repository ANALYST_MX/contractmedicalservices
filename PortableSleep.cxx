#include "PortableSleep.hxx"

void PortableSleep::sleepMillis(uint32_t ms)
{
#ifdef _WIN32
    Sleep(ms);
#else
    usleep(1000 * ms);
#endif
}

void PortableSleep::sleepSeconds(uint8_t s)
{
    sleepMillis((long)s * 1000);
}

void PortableSleep::sleepMinutes(uint8_t m)
{
    sleepMillis((long)m * 1000 * 60);
}

void PortableSleep::sleepHours(uint8_t h)
{
    sleepMillis((long) h * 1000 * 60 * 60);
}
