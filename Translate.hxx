#pragma once

#include <QObject>
#include <string>
#include <array>
#include <map>

#define SIZE_TRANSLIT 67

class Translate : public QObject
{
    Q_OBJECT
public:
    enum class LANG : int { ENG, RUS };
public:
    explicit Translate(QObject *parent = 0);
    static std::string translateToRus(std::string);
    static std::string translateToEng(std::string);
    static QString translateToRus(QString);
    static QString translateToEng(QString);
    static LANG getCurentLang(const std::string &);
    static LANG getCurentLang(QString);
private:
    static void replaceAll(std::string &, const std::string &, const std::string &);
private:
    static std::map<LANG, std::array<std::string, SIZE_TRANSLIT>> translit;

signals:

public slots:
};
