#pragma once

#include <QString>
#include <QSettings>
#include "App.hxx"

class Configure : public QObject
{
    Q_OBJECT

public:
    Configure(QObject *parent = nullptr);
    void save();
    QString getLoginAuth();
    QString getPasswordAuth();
    QString getBaseURLAuth();
    QString getDriverNameDB() const;
    QString getDatabaseNameDB() const;
    QString getUserNameDB() const;
    QString getPasswordDB() const;
    QString getHostNameDB() const;
    int getPortDB() const;
    QString getPathTempateFilePrint() const;
    QString getPathTempDirectory() const;
    int getCurrencyDecimalPlaces() const;
    QString getCurrencySymbol() const;
    int getCountMedicalServices() const;
    void setLoginAuth(const QString &);
    void setPasswordAuth(const QString &);
    void setBaseURLAuth(const QString &);
    void setDriverNameDB(const QString &);
    void setDatabaseNameDB(const QString &);
    void setUserNameDB(const QString &);
    void setPasswordDB(const QString &);
    void setHostNameDB(const QString &);
    void setPortDB(int);
    void setPathTempateFilePrint(const QString &);
    void setPathTempDirectory(const QString &);
    void setCurrencyDecimalPlaces(int);
    void setCurrencySymbol(const QString &);
    void setCountMedicalServices(int);

private:
    void load();
    QSettings *m_settings;
    bool auto_start;
    QString m_sLoginAuth;
    QString m_sPasswordAuth;
    QString m_sBaseURLAuth;
    QString m_sDriverNameDB;
    QString m_sDatabaseNameDB;
    QString m_sUserNameDB;
    QString m_sPasswordDB;
    QString m_sHostNameDB;
    QString m_sPathTempateFilePrint;
    QString m_sPathTempDirectory;
    QString m_sCurrencySymbol;
    int m_iCurrencyDecimalPlaces;
    int m_iCountMedicalServices;
    int m_iPortDB;
};

