#include "ContractNumerator.hxx"

ContractNumerator::ContractNumerator(QObject *parent)
    : QObject(parent)
{

}

QDate ContractNumerator::getStartDate() const
{
    return m_dateStartDate;
}

int ContractNumerator::getFromNumber() const
{
    return m_iFromNumber;
}

int ContractNumerator::getToNumber() const
{
    return m_iToNumber;
}

QString ContractNumerator::getSerial() const
{
    return m_sSerial;
}

int ContractNumerator::getNumberLength() const
{
    return m_iNumberLength;
}

void ContractNumerator::setStartDate(QDate startDate)
{
    m_dateStartDate = startDate;
}

void ContractNumerator::setFromNumber(int fromNumber)
{
    m_iFromNumber = fromNumber;
}

void ContractNumerator::setToNumber(int toNumber)
{
    m_iToNumber = toNumber;
}

void ContractNumerator::setSerial(const QString &serial)
{
    m_sSerial = serial;
}

void ContractNumerator::setNumberLength(int length)
{
    m_iNumberLength = length;
}
