#include "MainWindow.hxx"
#include "EnumHelpers.hxx"
#include "Translate.hxx"

#include <algorithm>

static enum class ClientType {PATIENT = 0,
                              REPRESENTATIVE} client;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    m_pAuth = new AuthPromed(this);
    m_pSession = new Session(m_pAuth, this);
    m_pPromed = new PersonPromed(m_pAuth, this);
    connect(m_pPromed, SIGNAL(ok()), SLOT(personInformationFound()));
    connect(m_pPromed, SIGNAL(failedOrgDepNotFound()), SLOT(personInformationFound()));
    connect(m_pPromed, SIGNAL(failedPersonNotFound()), SLOT(personInformationNotFound()));
    connect(m_pPromed, SIGNAL(error()), SLOT(personInformationNotFound()));
    db = DBConnection::instance();

    while(!checkConfig());

    view();

    m_pCart = new CartMedicalServiceList(this);
    searchUslugi();
}

void MainWindow::view()
{
    QWidget *pwgtWindow = new QWidget(this);
    pgrdLayout = new QGridLayout;
    pwgtWindow->setLayout(pgrdLayout);
    pgrdLayout->setMargin(5);
    pgrdLayout->setSpacing(5);

    QGroupBox *pgbxContractInfo = new QGroupBox;
    pgbxContractInfo->setStyleSheet("QGroupBox {  border: 0px;}");
    QFormLayout *pfrmContractLayout = new QFormLayout;
    pgbxContractInfo->setLayout(pfrmContractLayout);
    QPair<QLabel *, QDateEdit *> *contractDateField = createDateField(tr("Дата договора:"));
    m_pdateContractDateField = contractDateField->second;
    pfrmContractLayout->addRow(contractDateField->first, contractDateField->second);
    QPair<QLabel *, QCheckBox *> *contractPrintOutField = createCheckBox(tr("Вывод на печать:"), true);
    m_pchkPrintOutContract = contractPrintOutField->second;
    pfrmContractLayout->addRow(contractPrintOutField->first, contractPrintOutField->second);
    m_ptabPersons = new QTabWidget;
    pgrdLayout->addWidget(m_ptabPersons, 0, 0, 1, 2);

    /* Tab Client */
    QGroupBox *pgbxClientInfo = new QGroupBox;
    pgbxClientInfo->setStyleSheet("QGroupBox {  border: 0px;}");
    QFormLayout *pfrmSearchClientLayout = new QFormLayout;
    pgbxClientInfo->setLayout(pfrmSearchClientLayout);
    QPair<QLabel *, QLineEdit *> *clientSurnameField = createTextField(tr("Фамилия:"));
    m_ptxtSearchClientSurnameField = clientSurnameField->second;
    pfrmSearchClientLayout->addRow(clientSurnameField->first, clientSurnameField->second);
    QPair<QLabel *, QLineEdit *> *clientNameField = createTextField(tr("Имя:"));
    m_ptxtSearchClientNameField = clientNameField->second;
    pfrmSearchClientLayout->addRow(clientNameField->first, clientNameField->second);
    QPair<QLabel *, QLineEdit *> *clientPatronymicField = createTextField(tr("Отчество:"));
    m_ptxtSearchClientPatronymicField = clientPatronymicField->second;
    pfrmSearchClientLayout->addRow(clientPatronymicField->first, clientPatronymicField->second);
    QPair<QLabel *, QLineEdit *> *clientAddressField = createTextField(tr("Адрес проживания:"));
    m_ptxtSearchClientAddressField = clientAddressField->second;
    pfrmSearchClientLayout->addRow(clientAddressField->first, clientAddressField->second);
    QPair<QLabel *, QLineEdit *> *clientPhoneField = createTextField(tr("Телефон:"));
    m_ptxtSearchClientPhoneField = clientPhoneField->second;
    pfrmSearchClientLayout->addRow(clientPhoneField->first, clientPhoneField->second);
    QPair<QLabel *, QDateEdit *> *clientDateBirthdayField = createDateField(tr("Дата рождения:"));
    m_pdateSearchClientBirthdayField = clientDateBirthdayField->second;
    pfrmSearchClientLayout->addRow(clientDateBirthdayField->first, clientDateBirthdayField->second);
    QPair<QLabel *, QComboBox *> *clientDocumentTypeField = createComboBox(tr("Тип документа:"));
    m_pcboSearchClientDocumentTypeField = clientDocumentTypeField->second;
    for (int typeId : Document::types().keys())
    {
        m_pcboSearchClientDocumentTypeField->addItem(Document::type(typeId), QVariant::fromValue(typeId));
    }
    pfrmSearchClientLayout->addRow(clientDocumentTypeField->first, clientDocumentTypeField->second);
    QPair<QLabel *, QLineEdit *> *clientDocumentSerialField = createTextField(tr("Серия:"));
    m_ptxtSearchClientDocumentSerialField = clientDocumentSerialField->second;
    pfrmSearchClientLayout->addRow(clientDocumentSerialField->first, clientDocumentSerialField->second);
    QPair<QLabel *, QLineEdit *> *clientDocumentNumberField = createTextField(tr("Номер:"));
    m_ptxtSearchClientDocumentNumberField = clientDocumentNumberField->second;
    pfrmSearchClientLayout->addRow(clientDocumentNumberField->first, clientDocumentNumberField->second);
    QPair<QLabel *, QLineEdit *> *clientDocumentAuthorityField = createTextField(tr("Выдан:"));
    m_ptxtSearchClientDocumentAuthorityField = clientDocumentAuthorityField->second;
    pfrmSearchClientLayout->addRow(clientDocumentAuthorityField->first, clientDocumentAuthorityField->second);
    QPair<QLabel *, QDateEdit *> *clientDocumentDateOfIssueField = createDateField(tr("Дата выдачи:"));
    m_pdateSearchClientDateOfIssueField = clientDocumentDateOfIssueField->second;
    pfrmSearchClientLayout->addRow(clientDocumentDateOfIssueField->first, clientDocumentDateOfIssueField->second);

    /* Tab Representative */
    QGroupBox *pgbxRepresentativeInfo = new QGroupBox;
    pgbxRepresentativeInfo->setStyleSheet("QGroupBox {  border: 0px;}");
    QFormLayout *pfrmSearchRepresentativeLayout = new QFormLayout;
    pgbxRepresentativeInfo->setLayout(pfrmSearchRepresentativeLayout);
    QPair<QLabel *, QLineEdit *> *RepresentativeSurnameField = createTextField(tr("Фамилия:"));
    m_ptxtSearchRepresentativeSurnameField = RepresentativeSurnameField->second;
    pfrmSearchRepresentativeLayout->addRow(RepresentativeSurnameField->first, RepresentativeSurnameField->second);
    QPair<QLabel *, QLineEdit *> *RepresentativeNameField = createTextField(tr("Имя:"));
    m_ptxtSearchRepresentativeNameField = RepresentativeNameField->second;
    pfrmSearchRepresentativeLayout->addRow(RepresentativeNameField->first, RepresentativeNameField->second);
    QPair<QLabel *, QLineEdit *> *RepresentativePatronymicField = createTextField(tr("Отчество:"));
    m_ptxtSearchRepresentativePatronymicField = RepresentativePatronymicField->second;
    pfrmSearchRepresentativeLayout->addRow(RepresentativePatronymicField->first, RepresentativePatronymicField->second);
    QPair<QLabel *, QLineEdit *> *RepresentativeAddressField = createTextField(tr("Адрес проживания:"));
    m_ptxtSearchRepresentativeAddressField = RepresentativeAddressField->second;
    pfrmSearchRepresentativeLayout->addRow(RepresentativeAddressField->first, RepresentativeAddressField->second);
    QPair<QLabel *, QLineEdit *> *RepresentativePhoneField = createTextField(tr("Телефон:"));
    m_ptxtSearchRepresentativePhoneField = RepresentativePhoneField->second;
    pfrmSearchRepresentativeLayout->addRow(RepresentativePhoneField->first, RepresentativePhoneField->second);
    QPair<QLabel *, QDateEdit *> *RepresentativeDateBirthdayField = createDateField(tr("Дата рождения:"));
    m_pdateSearchRepresentativeBirthdayField = RepresentativeDateBirthdayField->second;
    pfrmSearchRepresentativeLayout->addRow(RepresentativeDateBirthdayField->first, RepresentativeDateBirthdayField->second);
    QPair<QLabel *, QComboBox *> *RepresentativeDocumentTypeField = createComboBox(tr("Тип документа:"));
    m_pcboSearchRepresentativeDocumentTypeField = RepresentativeDocumentTypeField->second;
    for (int typeId : Document::types().keys())
    {
        m_pcboSearchRepresentativeDocumentTypeField->addItem(Document::type(typeId), QVariant::fromValue(typeId));
    }
    pfrmSearchRepresentativeLayout->addRow(RepresentativeDocumentTypeField->first, RepresentativeDocumentTypeField->second);
    QPair<QLabel *, QLineEdit *> *RepresentativeDocumentSerialField = createTextField(tr("Серия:"));
    m_ptxtSearchRepresentativeDocumentSerialField = RepresentativeDocumentSerialField->second;
    pfrmSearchRepresentativeLayout->addRow(RepresentativeDocumentSerialField->first, RepresentativeDocumentSerialField->second);
    QPair<QLabel *, QLineEdit *> *RepresentativeDocumentNumberField = createTextField(tr("Номер:"));
    m_ptxtSearchRepresentativeDocumentNumberField = RepresentativeDocumentNumberField->second;
    pfrmSearchRepresentativeLayout->addRow(RepresentativeDocumentNumberField->first, RepresentativeDocumentNumberField->second);
    QPair<QLabel *, QLineEdit *> *RepresentativeDocumentAuthorityField = createTextField(tr("Выдан:"));
    m_ptxtSearchRepresentativeDocumentAuthorityField = RepresentativeDocumentAuthorityField->second;
    pfrmSearchRepresentativeLayout->addRow(RepresentativeDocumentAuthorityField->first, RepresentativeDocumentAuthorityField->second);
    QPair<QLabel *, QDateEdit *> *RepresentativeDocumentDateOfIssueField = createDateField(tr("Дата выдачи:"));
    m_pdateSearchRepresentativeDateOfIssueField = RepresentativeDocumentDateOfIssueField->second;
    pfrmSearchRepresentativeLayout->addRow(RepresentativeDocumentDateOfIssueField->first, RepresentativeDocumentDateOfIssueField->second);

    QGroupBox *pgbxUslugiInfo = new QGroupBox(this);
    QGridLayout *pgrpUslugiLayout = new QGridLayout;
    pgbxUslugiInfo->setLayout(pgrpUslugiLayout);
    QFormLayout *pfrmEditLayout = new QFormLayout;
    QPair<QLabel *, QLineEdit *> *idField = createTextField(tr("ID:"));
    m_ptxtEditUslugaIDField = idField->second;
    pfrmEditLayout->addRow(idField->first, idField->second);
    QPair<QLabel *, QLineEdit *> *codeField = createTextField(tr("Код:"));
    m_ptxtEditUslugaCodeField = codeField->second;
    pfrmEditLayout->addRow(codeField->first, codeField->second);
    QPair<QLabel *, QLineEdit *> *nameField = createTextField(tr("Наименование:"));
    m_ptxtEditUslugaNameField = nameField->second;
    pfrmEditLayout->addRow(nameField->first, nameField->second);
    QPair<QLabel *, QLineEdit *> *priceField = createTextField(tr("Цена:"));
    m_ptxtEditUslugaPriceField = priceField->second;
    pfrmEditLayout->addRow(priceField->first, priceField->second);
    QPair<QLabel *, QSpinBox *> *countField = createSpinField(tr("Кол-во:"), 1);
    m_pspbEditUslugaCountField = countField->second;
    pfrmEditLayout->addRow(countField->first, countField->second);
    QPair<QLabel *, QDateEdit *> *dateExpiresField = createDateField(tr("Срок:"));
    m_pdateEditUslugaDateExpiresField = dateExpiresField->second;
    pfrmEditLayout->addRow(dateExpiresField->first, dateExpiresField->second);
    pgrpUslugiLayout->addLayout(pfrmEditLayout, 0, 0, 1, 2);

    QPushButton *btnAdd = createButton(tr("Добавить"), SLOT(addUslugaToCartUslugiTable()));
    pgrpUslugiLayout->addWidget(btnAdd, 1, 0);
    QPushButton *btnRemove = createButton(tr("Удалить"), SLOT(removeUslugafromCartUslugiTable()));
    pgrpUslugiLayout->addWidget(btnRemove, 1, 1);

    QBoxLayout *pbxSearchUslugiLayout = new QBoxLayout(QBoxLayout::TopToBottom);
    QFormLayout *pfrmSearchUslugiLayout = new QFormLayout;
    QPair<QLabel *, QLineEdit *> *searchField = createTextField(tr("Поиск:"));
    m_ptxtSearchUslugiField = searchField->second;
    connect(m_ptxtSearchUslugiField, SIGNAL(textChanged(const QString &)), SLOT(searchUslugi()));
    pfrmSearchUslugiLayout->addRow(searchField->first, searchField->second);

    QStringList labelsUslugi;
    labelsUslugi << "ID" << "Код" << "Наименование" << "Цена";
    m_ptblFindedUslugiTable = createTable(0, labelsUslugi.size(), SLOT(selectUslugaFromFindedUslugiTable(int, int)));
    m_ptblFindedUslugiTable->setHorizontalHeaderLabels(labelsUslugi);
    m_ptblFindedUslugiTable->setColumnHidden(0, true);

    pbxSearchUslugiLayout->addLayout(pfrmSearchUslugiLayout, 0);
    pbxSearchUslugiLayout->addWidget(m_ptblFindedUslugiTable, 1);
    pgrpUslugiLayout->addLayout(pbxSearchUslugiLayout, 0, 2, 2, 1);

    QStringList labelsCart;
    labelsCart << "ID" << "Код" << "Наименование" << "Цена" << "Количество" << "Срок исполнения";
    m_ptblCartUslugiTable = createTable(0, labelsCart.size(), SLOT(selectUslugaFromCartUslugiTable(int, int)));
    m_ptblCartUslugiTable->setHorizontalHeaderLabels(labelsCart);
    m_ptblCartUslugiTable->setColumnHidden(0, true);
    pgrpUslugiLayout->addWidget(m_ptblCartUslugiTable, 2, 0, 9, 4);

    m_ptabPersons->addTab(pgbxClientInfo, "Пациент");
    m_ptabPersons->addTab(pgbxRepresentativeInfo, "Представитель");

    pgrdLayout->addWidget(pgbxContractInfo, 0, 0, 1, 2);
    pgrdLayout->addWidget(m_ptabPersons, 1, 0, 1, 2);
    m_pcmdSearchPerson = createButton(tr("Поиск по ФИО и ДР"), SLOT(findByFIOAndBirthday()));
    pgrdLayout->addWidget(m_pcmdSearchPerson, 2, 0, 1, 2);
    pgrdLayout->addWidget(pgbxUslugiInfo, 3, 0, 1, 2);

    pgrdLayout->addWidget(createButton(tr("Сброс"), SLOT(reset())), 4, 0);
    pgrdLayout->addWidget(createButton(tr("Печать"), SLOT(print())), 4, 1);

    m_plblTotalCostUslugi = createLabel(tr("Стоимость"));
    statusBar()->addWidget(m_plblTotalCostUslugi);

    translateToRus =
    {
        m_ptxtSearchClientSurnameField,
        m_ptxtSearchClientNameField,
        m_ptxtSearchClientPatronymicField,
        m_ptxtSearchRepresentativeSurnameField,
        m_ptxtSearchRepresentativeNameField,
        m_ptxtSearchRepresentativePatronymicField
    };

    std::for_each(translateToRus.cbegin(), translateToRus.cend(), [&](QObject *field) {
            field->installEventFilter(this);
    });

    createMenus();

    setCentralWidget(pwgtWindow);
}

MainWindow::~MainWindow()
{
}

void MainWindow::addUslugaToCartUslugiTable()
{
    QString id = m_ptxtEditUslugaIDField->text();
    if (m_pCart->contains(id))
    {
        m_pCart->remove(id);
    }
    MedicalServiceEntry *usluga = new MedicalServiceEntry(m_pCart);
    usluga->setCode(m_ptxtEditUslugaCodeField->text());
    usluga->setName(m_ptxtEditUslugaNameField->text());
    usluga->setPrice(m_ptxtEditUslugaPriceField->text().toDouble());
    usluga->setCount(m_pspbEditUslugaCountField->value());
    usluga->setDateExpires(m_pdateEditUslugaDateExpiresField->date());
    m_pCart->set(m_ptxtEditUslugaIDField->text(), usluga);
    showCartUslugi();
}

void MainWindow::removeUslugafromCartUslugiTable()
{
    m_pCart->remove(m_ptxtEditUslugaIDField->text());
    showCartUslugi();
}

void MainWindow::showSettingAuth(bool block)
{
    static SettingAuthWindow wgtSetting;
    if (!block)
    {
        wgtSetting.show();
    }
    else
    {
        wgtSetting.exec();
    }
}

void MainWindow::showSettingDB(bool block)
{
    static SettingDBWindow wgtSetting;
    if (!block)
    {
        wgtSetting.show();
    }
    else
    {
        wgtSetting.exec();
    }
}

void MainWindow::showSettingPrintFileTemplate(bool block)
{
    static SettingPrintWindow wgtSetting;
    if (!block)
    {
        wgtSetting.show();
    }
    else
    {
        wgtSetting.exec();
    }
}

void MainWindow::showSettingNumeratorInterval(bool block)
{
    static SettingNumeratorWindow wgtSetting;
    if (!block)
    {
        wgtSetting.show();
    }
    else
    {
        wgtSetting.exec();
    }
}

void MainWindow::createMenus()
{
    QMenuBar* mnuBar = new QMenuBar;
    QMenu *pmnu = new QMenu("&Menu");
    pmnu->addAction(tr("Setting auth"), this, SLOT(showSettingAuth()));
    pmnu->addAction(tr("Setting db"), this, SLOT(showSettingDB()));
    pmnu->addAction(tr("Setting print"), this, SLOT(showSettingPrintFileTemplate()));
    pmnu->addAction(tr("Setting interval"), this, SLOT(showSettingNumeratorInterval()));
    pmnu->addSeparator();
    pmnu->addAction(tr("&Exit"), this, SLOT(close()));
    mnuBar->addMenu(pmnu);
    pgrdLayout->setMenuBar(mnuBar);
}

QPair<QLabel *, QLineEdit *> *MainWindow::createTextField(const QString &label)
{
    QPair<QLabel *, QLineEdit *> *field = new QPair<QLabel *, QLineEdit *>;
    QLabel *lbl = createLabel(label);
    QLineEdit *txt = new QLineEdit;
    lbl->setBuddy(txt);
    field->first = lbl;
    field->second = txt;

    return field;
}

QPair<QLabel *, QCheckBox *> *MainWindow::createCheckBox(const QString &label, bool checked)
{
    QPair<QLabel *, QCheckBox *> *field = new QPair<QLabel *, QCheckBox *>;
    QLabel *lbl = createLabel(label);
    QCheckBox *chk = new QCheckBox;
    chk->setChecked(checked);
    lbl->setBuddy(chk);
    field->first = lbl;
    field->second = chk;

    return field;
}

QPair<QLabel *, QSpinBox *> *MainWindow::createSpinField(const QString &label, int min, int max)
{
    QPair<QLabel *, QSpinBox *> *field = new QPair<QLabel *, QSpinBox *>;
    QLabel *lbl = createLabel(label);
    QSpinBox *spb = new QSpinBox;
    lbl->setBuddy(spb);
    spb->setMinimum(min);
    spb->setMaximum(max);
    field->first = lbl;
    field->second = spb;

    return field;
}

QPair<QLabel *, QDateEdit *> *MainWindow::createDateField(const QString &label)
{
    QPair<QLabel *, QDateEdit *> *field = new QPair<QLabel *, QDateEdit *>;
    QLabel *lbl = createLabel(label);
    QDateEdit *dt = new QDateEdit(QDate::currentDate());
    lbl->setBuddy(dt);
    field->first = lbl;
    field->second = dt;

    return field;
}

QPair<QLabel *, QComboBox *> *MainWindow::createComboBox(const QString &label)
{
    QPair<QLabel *, QComboBox *> *field = new QPair<QLabel *, QComboBox *>;
    QLabel *lbl = createLabel(label);
    QComboBox *cbo = new QComboBox;
    lbl->setBuddy(cbo);
    field->first = lbl;
    field->second = cbo;

    return field;
}

QPushButton *MainWindow::createButton(const QString &str, const char *method)
{
    QPushButton *pcmd = new QPushButton(str);
    connect(pcmd, SIGNAL(clicked()), method);

    return pcmd;
}

QTableWidget *MainWindow::createTable(int rows, int columns, const char *method)
{
    QTableWidget *table = new QTableWidget(rows, columns);
    table->setSelectionBehavior(QAbstractItemView::SelectRows);
    table->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
    table->verticalHeader()->hide();
    table->setShowGrid(false);
    connect(table, SIGNAL(cellClicked(int, int)), method);

    return table;
}

QLabel *MainWindow::createLabel(const QString &text)
{
    QLabel *label = new QLabel(tr(text.toStdString().c_str()));

    return label;
}

void MainWindow::findByFIOAndBirthday()
{
    m_pcmdSearchPerson->setEnabled(false);
    client = static_cast<ClientType>(m_ptabPersons->currentIndex());
    if (client == ClientType::PATIENT)
    {
        m_pPromed->findByFIOAndBirthday(m_ptxtSearchClientSurnameField->text(),
                                        m_ptxtSearchClientNameField->text(),
                                        m_ptxtSearchClientPatronymicField->text(),
                                        m_pdateSearchClientBirthdayField->date());
    }
    else if (client == ClientType::REPRESENTATIVE)
    {
        m_pPromed->findByFIOAndBirthday(m_ptxtSearchRepresentativeSurnameField->text(),
                                        m_ptxtSearchRepresentativeNameField->text(),
                                        m_ptxtSearchRepresentativePatronymicField->text(),
                                        m_pdateSearchRepresentativeBirthdayField->date());
    }
}

void MainWindow::print()
{
    Contract contract(db->getDatabase(), Singleton<Configure>::instance()->getPathTempateFilePrint());
    Document *doc = new Document;
    doc->setAuthority(m_ptxtSearchClientDocumentAuthorityField->text());
    doc->setDateOfIssue(m_pdateSearchClientDateOfIssueField->date());
    doc->setSerial(m_ptxtSearchClientDocumentSerialField->text());
    doc->setNumber(m_ptxtSearchClientDocumentNumberField->text());
    doc->setType(m_pcboSearchClientDocumentTypeField->currentIndex());
    ContractNumerator *contractNumerator = ContractNumeratorCommands(db->getDatabase()).select(m_pdateContractDateField->date());
    Person *psn = new Person(&contract);
    psn->setSurname(m_ptxtSearchClientSurnameField->text());
    psn->setName(m_ptxtSearchClientNameField->text());
    psn->setPatronymic(m_ptxtSearchClientPatronymicField->text());
    psn->setBirthday(m_pdateSearchClientBirthdayField->date());
    psn->setAddress(m_ptxtSearchClientAddressField->text());
    psn->setPhone(m_ptxtSearchClientPhoneField->text());
    psn->setDocument(doc);
    if (!m_ptxtSearchRepresentativeSurnameField->text().isEmpty())
    {
        Document *docRepresentative = new Document;
        docRepresentative->setAuthority(m_ptxtSearchRepresentativeDocumentAuthorityField->text());
        docRepresentative->setDateOfIssue(m_pdateSearchRepresentativeDateOfIssueField->date());
        docRepresentative->setSerial(m_ptxtSearchRepresentativeDocumentSerialField->text());
        docRepresentative->setNumber(m_ptxtSearchRepresentativeDocumentNumberField->text());
        docRepresentative->setType(m_pcboSearchRepresentativeDocumentTypeField->currentIndex());
        Person *psnRepresentative = new Person(&contract);
        psnRepresentative->setSurname(m_ptxtSearchRepresentativeSurnameField->text());
        psnRepresentative->setName(m_ptxtSearchRepresentativeNameField->text());
        psnRepresentative->setPatronymic(m_ptxtSearchRepresentativePatronymicField->text());
        psnRepresentative->setBirthday(m_pdateSearchRepresentativeBirthdayField->date());
        psnRepresentative->setAddress(m_ptxtSearchRepresentativeAddressField->text());
        psnRepresentative->setPhone(m_ptxtSearchRepresentativePhoneField->text());
        psnRepresentative->setDocument(docRepresentative);
        psn->setRepresentative(psnRepresentative);
    }

    contract.setDateContract(m_pdateContractDateField->date());
    contract.setContractNumerator(contractNumerator);
    contract.setClient(psn);
    contract.setCurrencySymbol(Singleton<Configure>::instance()->getCurrencySymbol());
    contract.setCurrencyDecimalPlaces(Singleton<Configure>::instance()->getCurrencyDecimalPlaces());
    contract.setOutputPathDirectory(Singleton<Configure>::instance()->getPathTempDirectory());
    contract.setCountMedicalServices(Singleton<Configure>::instance()->getCountMedicalServices());
    m_ptblCartUslugiTable->setRowCount(0);
    for (auto it = m_pCart->begin(); it != m_pCart->end(); ++it)
    {
        contract.addUsluga(it.value()->clone());
    }
    try
    {
        contract.printPreview(m_pchkPrintOutContract->isChecked());
    } catch (...)
    {
    }

    showCartUslugi();
}

void MainWindow::reset()
{
    m_pdateContractDateField->setDate(QDate::currentDate());
    m_ptxtSearchClientSurnameField->clear();
    m_ptxtSearchClientNameField->clear();
    m_ptxtSearchClientPatronymicField->clear();
    m_ptxtSearchClientAddressField->clear();
    m_ptxtSearchClientPhoneField->clear();
    m_pdateSearchClientBirthdayField->setDate(QDate::currentDate());
    m_ptxtSearchClientDocumentAuthorityField->clear();
    m_ptxtSearchClientDocumentNumberField->clear();
    m_ptxtSearchClientDocumentSerialField->clear();
    m_pdateSearchClientDateOfIssueField->setDate(QDate::currentDate());

    m_ptxtSearchRepresentativeSurnameField->clear();
    m_ptxtSearchRepresentativeNameField->clear();
    m_ptxtSearchRepresentativePatronymicField->clear();
    m_ptxtSearchRepresentativeAddressField->clear();
    m_ptxtSearchRepresentativePhoneField->clear();
    m_pdateSearchRepresentativeBirthdayField->setDate(QDate::currentDate());
    m_ptxtSearchRepresentativeDocumentAuthorityField->clear();
    m_ptxtSearchRepresentativeDocumentNumberField->clear();
    m_ptxtSearchRepresentativeDocumentSerialField->clear();
    m_pdateSearchRepresentativeDateOfIssueField->setDate(QDate::currentDate());

    m_ptxtSearchUslugiField->clear();
    m_ptxtEditUslugaIDField->clear();
    m_ptxtEditUslugaCodeField->clear();
    m_ptxtEditUslugaNameField->clear();
    m_ptxtEditUslugaPriceField->clear();
    m_pCart->clear();

    showCartUslugi();
}

void MainWindow::showTotalCost()
{
    double totalCost = 0;
    for (auto it = m_pCart->begin(); it != m_pCart->end(); ++it)
    {
        totalCost += it.value()->getPrice() * it.value()->getCount();
    }
    m_plblTotalCostUslugi->setText(tr("Сумма: %1 руб.").arg(totalCost));
    m_plblTotalCostUslugi->setWordWrap(true);
}

void MainWindow::searchUslugi()
{
    MedicalServiceList *uslugi = new MedicalServiceList(this);
    uslugi->findMedicalServices(m_ptxtSearchUslugiField->text());
    showFindedUslugi(uslugi);
}

void MainWindow::showFindedUslugi(MedicalServiceList *uslugi)
{
    m_ptblFindedUslugiTable->setRowCount(0);
    for (auto i = 0; i < uslugi->rowCount(); ++i)
    {
        QTableWidgetItem *uslugaIdItem = new QTableWidgetItem(QString::number(uslugi->get(i)->key()));
        QTableWidgetItem *uslugaCodeItem = new QTableWidgetItem(uslugi->get(i)->getCode());
        QTableWidgetItem *uslugaNameItem = new QTableWidgetItem(uslugi->get(i)->getName());
        QTableWidgetItem *uslugaPriceItem = new QTableWidgetItem(QString::number(uslugi->get(i)->getPrice()));

        int row = m_ptblFindedUslugiTable->rowCount();
        m_ptblFindedUslugiTable->insertRow(row);
        m_ptblFindedUslugiTable->setItem(row, 0, uslugaIdItem);
        m_ptblFindedUslugiTable->setItem(row, 1, uslugaCodeItem);
        m_ptblFindedUslugiTable->setItem(row, 2, uslugaNameItem);
        m_ptblFindedUslugiTable->setItem(row, 3, uslugaPriceItem);
    }
    m_ptblFindedUslugiTable->setDragEnabled(true);
}

void MainWindow::showCartUslugi()
{
    m_ptblCartUslugiTable->setRowCount(0);
    for (auto it = m_pCart->begin(); it != m_pCart->end(); ++it)
    {
        QTableWidgetItem *uslugaIDItem = new QTableWidgetItem(it.key());
        QTableWidgetItem *uslugaCodeItem = new QTableWidgetItem(it.value()->getCode());
        QTableWidgetItem *uslugaNameItem = new QTableWidgetItem(it.value()->getName());
        QTableWidgetItem *uslugaPriceItem = new QTableWidgetItem(QString::number(it.value()->getPrice()));
        QTableWidgetItem *uslugaCountItem = new QTableWidgetItem(QString::number(it.value()->getCount()));
        QTableWidgetItem *uslugaDateExpiresItem = new QTableWidgetItem(it.value()->getDateExpires().toString("dd.MM.yyyy"));

        int row = m_ptblCartUslugiTable->rowCount();
        m_ptblCartUslugiTable->insertRow(row);
        m_ptblCartUslugiTable->setItem(row, 0, uslugaIDItem);
        m_ptblCartUslugiTable->setItem(row, 1, uslugaCodeItem);
        m_ptblCartUslugiTable->setItem(row, 2, uslugaNameItem);
        m_ptblCartUslugiTable->setItem(row, 3, uslugaPriceItem);
        m_ptblCartUslugiTable->setItem(row, 4, uslugaCountItem);
        m_ptblCartUslugiTable->setItem(row, 5, uslugaDateExpiresItem);
    }
    m_ptblCartUslugiTable->setDragEnabled(true);
    showTotalCost();
}

void MainWindow::selectUslugaFromFindedUslugiTable(int row, int /*column*/)
{
    m_ptxtEditUslugaIDField->setText(m_ptblFindedUslugiTable->item(row, 0)->text());
    m_ptxtEditUslugaCodeField->setText(m_ptblFindedUslugiTable->item(row, 1)->text());
    m_ptxtEditUslugaNameField->setText(m_ptblFindedUslugiTable->item(row, 2)->text());
    m_ptxtEditUslugaPriceField->setText(m_ptblFindedUslugiTable->item(row, 3)->text());
    m_pspbEditUslugaCountField->setValue(1);
}

void MainWindow::selectUslugaFromCartUslugiTable(int row, int /*column*/)
{
    QString id = m_ptblCartUslugiTable->item(row, 0)->text();
    MedicalServiceEntry *usluga = m_pCart->get(id);
    m_ptxtEditUslugaIDField->setText(id);
    m_ptxtEditUslugaCodeField->setText(usluga->getCode());
    m_ptxtEditUslugaNameField->setText(usluga->getName());
    m_ptxtEditUslugaPriceField->setText(QString::number(usluga->getPrice()));
    m_pspbEditUslugaCountField->setValue(usluga->getCount());
    m_pdateEditUslugaDateExpiresField->setDate(usluga->getDateExpires());
}

void MainWindow::personInformationFound()
{
    m_pcmdSearchPerson->setEnabled(true);
    Person *person = m_pPromed->getSearchResult();
    if (client == ClientType::PATIENT)
    {
        m_ptxtSearchClientSurnameField->setText(person->getSurname());
        m_ptxtSearchClientNameField->setText(person->getName());
        m_ptxtSearchClientPatronymicField->setText(person->getPatronymic());
        m_pdateSearchClientBirthdayField->setDate(person->getBirthday());
        m_ptxtSearchClientAddressField->setText(person->getAddress());
        m_ptxtSearchClientPhoneField->setText(person->getPhone());
        m_pdateSearchClientBirthdayField->setDate(person->getBirthday());
        Document *document = person->getDocument();
        m_ptxtSearchClientDocumentSerialField->setText(document->getSerial());
        m_ptxtSearchClientDocumentNumberField->setText(document->getNumber());
        m_ptxtSearchClientDocumentAuthorityField->setText(document->getAuthority());
        m_pdateSearchClientDateOfIssueField->setDate(document->getDateOfIssue());
        m_pcboSearchClientDocumentTypeField->setCurrentIndex(document->getType().first);
    }
    else if (client == ClientType::REPRESENTATIVE)
    {
        m_ptxtSearchRepresentativeSurnameField->setText(person->getSurname());
        m_ptxtSearchRepresentativeNameField->setText(person->getName());
        m_ptxtSearchRepresentativePatronymicField->setText(person->getPatronymic());
        m_pdateSearchRepresentativeBirthdayField->setDate(person->getBirthday());
        m_ptxtSearchRepresentativeAddressField->setText(person->getAddress());
        m_ptxtSearchRepresentativePhoneField->setText(person->getPhone());
        m_pdateSearchRepresentativeBirthdayField->setDate(person->getBirthday());
        Document *document = person->getDocument();
        m_ptxtSearchRepresentativeDocumentSerialField->setText(document->getSerial());
        m_ptxtSearchRepresentativeDocumentNumberField->setText(document->getNumber());
        m_ptxtSearchRepresentativeDocumentAuthorityField->setText(document->getAuthority());
        m_pdateSearchRepresentativeDateOfIssueField->setDate(document->getDateOfIssue());
        m_pcboSearchRepresentativeDocumentTypeField->setCurrentIndex(document->getType().first);
    }
}

void MainWindow::personInformationNotFound()
{
    m_pcmdSearchPerson->setEnabled(true);
    QMessageBox msgBox;
    if (client == ClientType::PATIENT)
    {
        msgBox.setText(tr("Информация по пациенту не найдена.\nЗаполните со слов клиента."));
    }
    else if (client == ClientType::REPRESENTATIVE)
    {
        msgBox.setText(tr("Информация по предаставителю не найдена.\nЗаполните со слов клиента."));
    }
    msgBox.exec();
}

bool MainWindow::checkConfig()
{
    bool ok = true;
    try
    {
        if (!db->isOpen())
        {
            db->connect();
        }
    }
    catch (SqlDatabaseException &)
    {
        showSettingDB(true);
        ok = false;
    }
    if (Singleton<Configure>::instance()->getLoginAuth().isEmpty() ||
            Singleton<Configure>::instance()->getBaseURLAuth().isEmpty() ||
            Singleton<Configure>::instance()->getPasswordAuth().isEmpty())
    {
        showSettingAuth(true);
        ok = false;
    }
    if (Singleton<Configure>::instance()->getPathTempateFilePrint().isEmpty() ||
            Singleton<Configure>::instance()->getPathTempDirectory().isEmpty())
    {
        showSettingPrintFileTemplate(true);
        ok = false;
    }
    return ok;
}

bool MainWindow::eventFilter(QObject *obj, QEvent *event)
{
    if (event->type() == QEvent::KeyRelease)
    {
        for (auto &field : translateToRus)
        {
            if (obj == field)
            {
                auto pos = ((QLineEdit *)field)->cursorPosition();
                ((QLineEdit *)field)->setText(Translate::translateToRus(((QLineEdit *)field)->text()));
                ((QLineEdit *)field)->setCursorPosition(pos);
                break;
            }
        }
    }
    // pass the event on to the parent class
    return QMainWindow::eventFilter(obj, event);
}
