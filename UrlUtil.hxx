#pragma once

#include <string>
#include <algorithm>
#include <cstring>
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <sstream>

enum UrlParserError
{
    Ok = 0,
    Uninitialized,
    NoUrlCharacter,
    InvalidSchemeName,
    NoDoubleSlash,
    NoAtSign,
    UnexpectedEndOfLine,
    NoSlash
};

class UrlUtil
{
public:
    UrlUtil();

    bool isValid() const;
    bool parsePort(int *) const;

    std::string getScheme() const;
    std::string getHost() const;
    std::string getPort() const;
    std::string getPath() const;
    std::string getQuery() const;
    std::string getFragment() const;
    std::string getUserName() const;
    std::string getPassword() const;
    UrlParserError getErrorCode() const;

    static UrlUtil parseURL(const std::string &);
    static bool isSchemeValid(const std::string &);
    static std::string encodeURL(const std::string &);
    static std::string decodeURL(const std::string &);

private:
    explicit UrlUtil(UrlParserError);
    UrlParserError m_statusCode;
    std::string m_sScheme;
    std::string m_sHost;
    std::string m_sPort;
    std::string m_sPath;
    std::string m_sQuery;
    std::string m_sFragment;
    std::string m_sUserName;
    std::string m_sPassword;
};
